import React from 'react';

const MockVictoryChart = ({ children, ...props }) => (
  <div {...props}>
    {/* Render children here */}
  </div>
);

export default MockVictoryChart;
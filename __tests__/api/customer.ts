import { useQuery } from '@tanstack/react-query';

export function GetPortfolioStocks() {
  return useQuery({
    queryFn: () => {
      return {
        data: {
          className: 'INSTANT-POSITION-SUMMARY',
          timestamp: '28.03.2023 11:33:57',
          instantPosSum: {
            instantPosSumItems: {
              instantPosSumItemList: [
                {
                  name: 'ABIBP',
                  balanceType: '',
                  value: '0.0000',
                  balance: '472.0000',
                  balanceT1: '472.0000',
                  balanceT2: '472.0000',
                  price: '0.0000',
                  change: '0.00',
                  cost: '0.0000',
                  ratio: '0',
                  potProfit: '0.0000',
                  typeCode: 'ISEQ',
                  typeName: '',
                  totalStock: '472.0000',
                  potProfitRatio: '0.00',
                },
                {
                  name: 'Aydem',
                  balanceType: '',
                  value: '5220.3200',
                  balance: '1106.0000',
                  balanceT1: '1106.0000',
                  balanceT2: '1106.0000',
                  price: '4.7200',
                  change: '-1.46',
                  cost: '5.1300',
                  ratio: '0',
                  potProfit: '-453.4600',
                  typeCode: 'ISEQ',
                  typeName: '',
                  totalStock: '1106.0000',
                  potProfitRatio: '-7.99',
                },
                {
                  name: 'AKBNK',
                  balanceType: '',
                  value: '384.3000',
                  balance: '21.0000',
                  balanceT1: '21.0000',
                  balanceT2: '21.0000',
                  price: '18.3000',
                  change: '-0.27',
                  cost: '0.0000',
                  ratio: '0',
                  potProfit: '384.3000',
                  typeCode: 'ISEQ',
                  typeName: '',
                  totalStock: '21.0000',
                  potProfitRatio: '0.00',
                },
                {
                  name: 'ASELS',
                  balanceType: '',
                  value: '334.2000',
                  balance: '6.0000',
                  balanceT1: '6.0000',
                  balanceT2: '6.0000',
                  price: '55.7000',
                  change: '-1.68',
                  cost: '0.0000',
                  ratio: '0',
                  potProfit: '334.2000',
                  typeCode: 'ISEQ',
                  typeName: '',
                  totalStock: '6.0000',
                  potProfitRatio: '0.00',
                },
                {
                  name: 'Isctr',
                  balanceType: '',
                  value: '0.0000',
                  balance: '500.0000',
                  balanceT1: '500.0000',
                  balanceT2: '500.0000',
                  price: '0.0000',
                  change: '-100.00',
                  cost: '0.0000',
                  ratio: '0',
                  potProfit: '0.0000',
                  typeCode: 'ISEQ',
                  typeName: '',
                  totalStock: '500.0000',
                  potProfitRatio: '0.00',
                },
                {
                  name: 'DOHOL',
                  balanceType: '',
                  value: '18.1600',
                  balance: '2.0000',
                  balanceT1: '2.0000',
                  balanceT2: '2.0000',
                  price: '9.0800',
                  change: '-1.73',
                  cost: '0.0000',
                  ratio: '0',
                  potProfit: '18.1600',
                  typeCode: 'ISEQ',
                  typeName: '',
                  totalStock: '2.0000',
                  potProfitRatio: '0.00',
                },
                {
                  name: 'EKGYO',
                  balanceType: '',
                  value: '66.4000',
                  balance: '10.0000',
                  balanceT1: '10.0000',
                  balanceT2: '10.0000',
                  price: '6.6400',
                  change: '-3.49',
                  cost: '0.0000',
                  ratio: '0',
                  potProfit: '66.4000',
                  typeCode: 'ISEQ',
                  typeName: '',
                  totalStock: '10.0000',
                  potProfitRatio: '0.00',
                },
                {
                  name: 'FROTO',
                  balanceType: '',
                  value: '20031.0000',
                  balance: '33.0000',
                  balanceT1: '33.0000',
                  balanceT2: '33.0000',
                  price: '607.0000',
                  change: '0.60',
                  cost: '0.0000',
                  ratio: '0',
                  potProfit: '20031.0000',
                  typeCode: 'ISEQ',
                  typeName: '',
                  totalStock: '33.0000',
                  potProfitRatio: '0.00',
                },
                {
                  name: 'GARAN',
                  balanceType: '',
                  value: '-676.0000',
                  balance: '-25.0000',
                  balanceT1: '-25.0000',
                  balanceT2: '-25.0000',
                  price: '27.0400',
                  change: '0.75',
                  cost: '0.0000',
                  ratio: '0',
                  potProfit: '-676.0000',
                  typeCode: 'ISEQ',
                  typeName: '',
                  totalStock: '-25.0000',
                  potProfitRatio: '0.00',
                },
                {
                  name: 'Egeen',
                  balanceType: '',
                  value: '2.4900',
                  balance: '3.0000',
                  balanceT1: '3.0000',
                  balanceT2: '3.0000',
                  price: '0.8300',
                  change: '-2.35',
                  cost: '0.0000',
                  ratio: '0',
                  potProfit: '2.4900',
                  typeCode: 'ISEQ',
                  typeName: '',
                  totalStock: '3.0000',
                  potProfitRatio: '0.00',
                },
                {
                  name: 'KRDMD',
                  balanceType: '',
                  value: '190.1000',
                  balance: '10.0000',
                  balanceT1: '10.0000',
                  balanceT2: '10.0000',
                  price: '19.0100',
                  change: '-2.01',
                  cost: '0.0000',
                  ratio: '0',
                  potProfit: '190.1000',
                  typeCode: 'ISEQ',
                  typeName: '',
                  totalStock: '10.0000',
                  potProfitRatio: '0.00',
                },
                {
                  name: 'MGROS',
                  balanceType: '',
                  value: '338.0000',
                  balance: '2.0000',
                  balanceT1: '2.0000',
                  balanceT2: '2.0000',
                  price: '169.0000',
                  change: '0.84',
                  cost: '0.0000',
                  ratio: '0',
                  potProfit: '338.0000',
                  typeCode: 'ISEQ',
                  typeName: '',
                  totalStock: '2.0000',
                  potProfitRatio: '0.00',
                },
                {
                  name: 'Aksa',
                  balanceType: '',
                  value: '76.5000',
                  balance: '3.0000',
                  balanceT1: '3.0000',
                  balanceT2: '3.0000',
                  price: '25.5000',
                  change: '-2.37',
                  cost: '0.0000',
                  ratio: '0',
                  potProfit: '76.5000',
                  typeCode: 'ISEQ',
                  typeName: '',
                  totalStock: '3.0000',
                  potProfitRatio: '0.00',
                },
                {
                  name: 'THYAO',
                  balanceType: '',
                  value: '618.0000',
                  balance: '5.0000',
                  balanceT1: '5.0000',
                  balanceT2: '5.0000',
                  price: '123.6000',
                  change: '-1.36',
                  cost: '135.7000',
                  ratio: '0',
                  potProfit: '-60.5000',
                  typeCode: 'ISEQ',
                  typeName: '',
                  totalStock: '5.0000',
                  potProfitRatio: '-8.92',
                },
              ],
            },
            equityTotalAmountList: [
              {
                name: 'Hisse Toplamı',
                amount: '26603.47',
                rate: '0.0',
              },
              {
                name: 'Anlık toplam kar/zarar(hisse)',
                amount: '20251.19',
                rate: '0.0',
              },
            ],
          },
        },
      };
    },
  });
}

export function PostLogin() {
  return useQuery({
    queryFn: () => {
      return {
        "data": {
          "refText": "000000000002307",
          "userId": "caf688e8-0c6e-4d29-81e3-12e99dd1c7ab",
          "subAccounts": {
            "subAccountList": [
              {
                "subAccountNumber": "3533491-100*",
                "subAccountName": "EQ",
                "subAccountChannel": "ADK",
                "comRate": 9.0E-5,
                "minCom": 0.05
              }
            ]
          }
        }
      };
    },
  });
}

export function AddConditionalOrder() {
  return useQuery({
    queryFn: () => {
      return {
        "data": {
          "conditionalOrder": {
            "message": " Referans Numaranız: A00010;0000-36OX3D-IET"
          }
        }
      };
    },
  });
}

export function AvailableDayInfo() {
  return useQuery({
    queryFn: () => {
      return {
        "data": {
          "availableDayInfo": {
            "dayInfo": {
              "sessionDate": "04.04.2023 00:00:00",
              "activeSession": "1"
            }
          }
        }
      };
    },
  });
}

export function ConditionalOrderList() {
  return useQuery({
    queryFn: () => {
      return {
        "data": {
          "conditionalOrders": {
            "conditionalOrderList": [
              {
                "transactionId": "0000-36OX36-IET",
                "transactionExtId": "A00003",
                "initialMarketSessionDate": "4/4/2023 12:00:00 AM",
                "equityName": "TTKOM",
                "debitCredit": "CREDIT",
                "units": "10.000000",
                "price": "98.000000",
                "transactionTypeName": "LOT",
                "timeInForce": "0",
                "conditionFinInstName": "ARCLK",
                "conditionType": "1",
                "conditionPrice": "8.980000",
                "description": "",
                "orderSend": "0",
                "endingMarketSessionDate": "4/4/2023 12:00:00 AM",
                "orderStatus": "Koşul bekleniyor.",
                "orderStatusGrouped": "1"
              }
            ]
          }
        }
      };
    },
  });
}

export function DeleteConditionalOrder() {
  return useQuery({
    queryFn: () => {
      return {
        "data": {
          "deleteCondOrderMessage": "SILMEOK"
        }
      };
    },
  });
}

export function EquityChainOrderDelete() {
  return useQuery({
    queryFn: () => {
      return {
        "data": {
          " eqOrderDeleteGroups": {
            " eqOrderDeleteGroup": {
              "ordersMessage": "Kayıt Silindi"
            }
          }
        }
      };
    },
  });
}

export function EquityChainOrderList() {
  return useQuery({
    queryFn: () => {
      return {
        "data": {
          "chainOrderListGroup": {
            "chainOrdersList": {
              "chainOrderList": [
                {
                  "chainNo": "2568",
                  "chainStatus": "SENT",
                  "orderSequenceNumber": "1",
                  "transactionId": "0000-36OX3O-IET",
                  "transactionExtId": "A00005",
                  "parentTransactionId": "",
                  "parentTransactionExtId": "",
                  "finInstName": "GARAN",
                  "price": "28.280000",
                  "debitCredit": "CREDIT",
                  "units": "5.000000",
                  "realizedUnits": "0",
                  "orderSend": "1",
                  "created": "4/5/2023 2:09:26 PM",
                  "transTypeName": "LOT",
                  "orderSession": "1",
                  "id": "0",
                  "parentId": "-1",
                  "timeInForce": "0",
                  "orderStatusGrouped": "1"
                }
              ]
            }
          }
        }
      };
    },
  });
}

export function EquityChainOrderUpdate() {
  return useQuery({
    queryFn: () => {
      return {
        "data": {
          "chainOrderGroup": {
            "chainOrderItems": {
              "mainTransactionId": "0000-36OL2I-IET",
              "mainPrice": "119.000000",
              "mainUnits": "3.000000",
              "transactionId": "0000-36OL2J-IET",
              "price": "117.40",
              "units": "2"
            }
          }
        }
      };
    },
  });
}

export function EquityDailyTransactions() {
  return useQuery({
    queryFn: () => {
      return {
        "data": {
          "transactions": {
            "transactionList": [
              {
                "id": "1",
                "valueDate": "2022-08-13",
                "equity": "GARAN",
                "unit": "2.000",
                "price": "17.70",
                "buySell": "A",
                "comm": "0.07",
                "bsmv": "0.00",
                "cost": "17.700",
                "amount": "35.40",
                "type": "H A/S",
                "desc": "Hisse Alış"
              },
              {
                "id": "2",
                "valueDate": "2022-08-13",
                "equity": "GARAN",
                "unit": "2.000",
                "price": "17.70",
                "buySell": "A",
                "comm": "0.07",
                "bsmv": "0.00",
                "cost": "17.700",
                "amount": "35.40",
                "type": "H A/S",
                "desc": "Hisse Alış"
              }
            ]
          }
        }
      };
    },
  });
}

export function EquityOrderModify() {
  return useQuery({
    queryFn: () => {
      return {
        "data": {
          "message": "IYILESOK"
        }
      };
    },
  });
}

export function EquityOrderResultForm() {
  return useQuery({
    queryFn: () => {
      return {
        "data": {
          "summary": {
            "accountExtId": "3676449-103",
            "address": "",
            "annualSettlementBsmv": "0",
            "annualSettlementExpense": "0",
            "annualSettlementTotal": "0",
            "creditBsmv": "0",
            "creditExpense": "0",
            "creditTotal": "0",
            "custodyBsmv": "0",
            "custodyExpense": "0",
            "custodyTotal": "0",
            "customerName": "Birey-01FJSX",
            "district": "",
            "divName": "EGE ŞUBESİ",
            "equityBsmv": "0.028100",
            "equityExpense": "0.471900",
            "equityTotal": "0.500000",
            "fincBsmv": "0",
            "fincExpense": "0",
            "fincTotal": "0",
            "lateBsmv": "0",
            "lateExpense": "0",
            "lateTotal": "0",
            "mmBsmv": "0",
            "mmExpense": "0",
            "mmTotal": "0",
            "orderNo": "7",
            "organization": "Garanti Yatırım Menkul Kıymetler A.Ş.",
            "organizationAddress": "Etiler Mah. Tepecik Yolu Demirkent Sok. No:1 34337 Etiler/Beşiktaş-ISTANBUL Tic.Sic.No: 235103 Mersis No: 0-3890-0068-3300011 www.garantibbvayatirim.com.tr",
            "otherBsmv": "0",
            "otherExpense": "0",
            "otherTotal": "0",
            "pbsmv": "0",
            "pexpense": "0",
            "ptotal": "0",
            "reverseRepoExpense": "0",
            "slBsmv": "0",
            "slExpense": "0",
            "slTotal": "0",
            "statementBsmv": "0",
            "statementExpense": "0",
            "statementTotal": "0",
            "taxInfo": " / ",
            "taxNo": "",
            "total": "0.50",
            "trnDate": "25.01.2023",
            "trnDateStr": "25/1/2023",
            "warrantBsmv": "0",
            "warrantExpense": "0",
            "warrantTotal": "0"
          },
          "transactions": [
            {
              "amount": "833.00",
              "channel": "GB INTERNET SUBE",
              "deputy": "",
              "imkbDebitCredit": "Alış",
              "name": "EKGYO",
              "orderNo": "7",
              "pfyDebitCredit": "",
              "price": "8.330000",
              "taxTotal": "0",
              "transCommission": "0",
              "trnDate": "25.01.2023",
              "type": "EQUITY",
              "units": "100.00",
              "valueDate": "25.01.2023"
            }
          ]
        }
      };
    },
  });
}

export function EquityOrder() {
  return useQuery({
    queryFn: () => {
      return {
        "data": {
          "orderGroup": {
            "message": "Referans Numaranız: A06201 - HISSEOK",
            "transactionId": "0000-36OX2I-IET",
            "transactionExtId": "A06201",
            "valueDate": "03.04.2023",
            "transactionDate": "03.04.2023 12:02"
          }
        }
      };
    },
  });
}

export function HcpEquityStock() {
  return useQuery({
    queryFn: () => {
      return {
        "data": {
          "equityStockItems": {
            "equityStockItemList": [
              {
                "equity": "GARAN",
                "balance": "14"
              }
            ]
          },
          "settlementDate": ""
        }
      };
    },
  });
}

export function HcpTradeLimit() {
  return useQuery({
    queryFn: () => {
      return {
        "data": {
          "tradeLimit": "0"
        }
      };
    },
  });
}

export function PeriodicOrderAdd() {
  return useQuery({
    queryFn: () => {
      return {
        "data": {
          "groups": {
            " periodicOrderGroupList": {
              "ordersMessage": "Süreli emir talebiniz başarıyla alınmıştır.Süreli emir Id:0000-36NPMU-IET."
            }
          }
        }
      };
    },
  });
}

export function PeriodicOrderCancel() {
  return useQuery({
    queryFn: () => {
      return {
        "data": {
          " groups": {
            "periodicOrderGroupList": {
              "ordersMessage": "Süreli emir talebiniz başarıyla alınmıştır.Süreli emir Id:0000-36NPMU-IET.",
              "ordersError": ""
            }
          }
        }
      };
    },
  });
}

export function PeriodicOrderList() {
  return useQuery({
    queryFn: () => {
      return {
        "data": {
          "groups": {
            "periodicOrderGroupList": [
              {
                "periodTransactionId": "0000-36OL15-IET",
                "customerNo": "3676409",
                "customerName": "AYLA ÇETİN",
                "accountNo": "3676409-100",
                "financialInstitutionName": "SAHOL",
                "units": "7",
                "realizedUnits": "0",
                "price": "40.240000",
                "debitCredit": "CREDIT",
                "result": "ORDER_SEND",
                "resultDescription": "Emir Gönderildi",
                "resultDate": "28.03.2023 15:54:40",
                "active": "1",
                "orderReferences": "A00051",
                "orderDate": "28.03.2023 00:00:00",
                "orderSession": "1",
                "sessionCount": "14",
                "sessionsExecuted": "1",
                "created": "28.03.2023 15:54:40",
                "orderStatusGrouped": "4"
              }
            ]
          }
        }
      };
    },
  });
}

export function PeriodicOrderUpdate() {
  return useQuery({
    queryFn: () => {
      return {
        "data": {
          "groups": {
            "periodicOrderGroupList": [
              {
                "ordersMessage": "Süreli emir güncelleme talebiniz başarıyla alınmıştır."
              }
            ]
          }
        }
      };
    },
  });
}

export function TodaysTransactions() {
  return useQuery({
    queryFn: () => {
      return {
        "data": {
          "todaysTransactions": {
            "equityOrders": {
              "equityOrderList": [
                {
                  "orderStatusGrouped": "1",
                  "atpRef": "A06208",
                  "ticker": "AGYO",
                  "buySell": "Satış",
                  "orderSize": "11.000000",
                  "remainingSize": "11.000000",
                  "price": "0",
                  "amount": "46.970000",
                  "transactionTime": "03.04.2023 00:00:00",
                  "valor": "03.04.2023",
                  "status": "11101",
                  "session": "1",
                  "validity": "0",
                  "waitingPrice": "4.270000",
                  "timeTransaction": "03.04.2023 13:40:28",
                  "timeValor": "03.04.2023 00:00:00",
                  "description": "İletildi",
                  "transactionId": "0000-36OX2P-IET",
                  "fillUnit": "0",
                  "equityStatusDescription": "WAITING",
                  "equityTransactionType": "LOT",
                  "shortFall": "0",
                  "createdBy": "0000T3-Pozisyonu",
                  "timeInForce": "0",
                  "expireDate": "03.04.2023",
                  "takeProfitStopLossOrderId": "",
                  "takeProfitPrice": "0",
                  "sellPrice": "0",
                  "maxFloor": "0",
                  "fillAmount": "0.00",
                  "lastUpdate": "01.01.0001 00:00:00"
                }
              ],
              "waitingOrderSum": "1601.12",
              "realizedOrderSum": "111.65"
            }
          }
        }
      };
    },
  });
}

export function UpdateConditionalOrder() {
  return useQuery({
    queryFn: () => {
      return {
        "data": {
          "conditionalOrder": {
            "message": " Referans Numaranız: A00014;0000-36OX3H-IET"
          }
        }
      };
    },
  });
}

export function ViopOrder() {
  return useQuery({
    queryFn: () => {
      return {
        "data": {
          "viopOrderGroupList": [
            {
              "ordersMessage": "Emriniz talep olarak alınmıştır. Referans numaranız:19U-0M2LO"
            }
          ]
        }
      };
    },
  });
}

export function ViopImproveOrder() {
  return useQuery({
    queryFn: () => {
      return {
        "data": {
          "customerTransactionsGroupList": [
            {
              "contract": "F_XU0300423",
              "shortLong": "Uzun",
              "units": "1",
              "leftUnits": "0",
              "price": "5500.0000",
              "transactionId": "19U-0M2LO",
              "sessionType": "1",
              "sessionTypeDesc": "Normal",
              "transactionDate": "10.04.2023 00:00:00",
              "transactionEndDate": "10.04.2023 00:00:00",
              "transactionEndDateType": "DAY",
              "transactionUnitNominal": "10.000000",
              "transactionUnit": "1",
              "transactionExId": "19U-0M2LO",
              "description": "Emir numara almadan iptal edilmiş.",
              "orderTime": "10.04.2023 10:01:47",
              "validity": "Günlük",
              "financialInstitutionId": "0000-003POH-FIN",
              "orderType": "KPY",
              "priceType": "Limitli",
              "priceTypeName": "LMT",
              "info": "Emir numara almadan iptal edilmiş.",
              "priceCondition": "0.0000",
              "realizedUnits": "0",
              "priceAverage": "0",
              "transStatusName": "CANCEL_ORDER",
              "triggerPriceTypeCode": "",
              "triggerPriceType": "",
              "triggerFinancialInstitutionName": "",
              "timeInForce": "DAY",
              "ordType": "LMT",
              "lastFillTime": "01.01.0001 00:00:00",
              "commission": "0"
            }
          ]
        }
      };
    },
  });
}

export function ViopCancelOrder() {
  return useQuery({
    queryFn: () => {
      return {
        "data": {
          "viopCancelOrderGroupList": [
            {
              "ordersMessage": "Emir iptal talebiniz basariyla alinmistir."
            }
          ]
        }
      };
    },
  });
}

export function AvailableBusinessDay() {
  return useQuery({
    queryFn: () => {
      return {
        "data": {
          "availableBusinessDayGroup": {
            "day": "10",
            "month": "4",
            "year": "2023"
          }
        }
      };
    },
  });
}

export function ViopCustomerOverall() {
  return useQuery({
    queryFn: () => {
      return {
        "data": {
          "custOverallGroupList": [
            {
              "contract": "F_AEFES0322",
              "contractName": "F_AEFES0322",
              "shortLong": "Uzun",
              "units": "600",
              "putCall": "",
              "shortBalance": "0",
              "longBalance": "600.000000",
              "openPositionTotal": "0",
              "exerciseUnits": "0",
              "waitingExerciseUnits": "0",
              "unitNominal": "100.000000",
              "totalCost": "20380.0800",
              "totalCostWithUnitNominal": "2038008.0000",
              "averageCost": "33.9668",
              "profit": "0",
              "positionVolume": "0.00",
              "financialInterestsId": "0000-003FOS-FIN",
              "dailyProfitLoss": "0.00",
              "potentialProfit": "-2038008.00",
              "profitLoss": "0.00",
              "price": "0",
              "lastPrice": "0",
              "notionalAmount": "0",
              "potentialProfitPercent": "-100.00",
              "dailyProfitLossPercent": "0.00"
            }
          ]
        }
      };
    },
  });
}

export function ViopCollateral() {
  return useQuery({
    queryFn: () => {
      return {
        "data": {
          "collateralInfoGroupList": [
            {
              "description": "Scan değeri",
              "value": "0"
            },
            {
              "description": "Net Opsiyon Değeri",
              "value": "0"
            },
            {
              "description": "Yayılma Maliyeti",
              "value": "0"
            },
            {
              "description": "Kar-Zarar",
              "value": "0"
            },
            {
              "description": "Kar-Zarar",
              "value": "0"
            },
            {
              "description": "Teminat Tamamlama Çağrısı Miktarı",
              "value": "0"
            },
            {
              "description": "Teslimat Maliyeti",
              "value": "0"
            },
            {
              "description": "Çekilebilir Teminat",
              "value": "0"
            },
            {
              "description": "Başlangıç Teminatı",
              "value": "0"
            },
            {
              "description": "Sürdürme Teminatı",
              "value": "0"
            },
            {
              "description": "Anlık Teminat Tamamlama Çağrısı Tutarı",
              "value": "0"
            },
            {
              "description": "Takas Nakit Dışı Teminatı",
              "value": "0"
            },
            {
              "description": "Takas Nakit Dışı Kurum Teminatı",
              "value": "0"
            },
            {
              "description": "Önceki Teminat Tamamlama Çağrısı Miktarı",
              "value": "0"
            },
            {
              "description": "Bulundurması Gereken Teminat",
              "value": "0"
            },
            {
              "description": "Risk seviyesi",
              "value": "0"
            },
            {
              "description": "Scan Riski",
              "value": "0"
            },
            {
              "description": "Takastaki Teminat",
              "value": "0"
            },
            {
              "description": "Çarpılmış Hesaplanan Teminat",
              "value": "0"
            },
            {
              "description": "Kurum Teminat",
              "value": "0"
            },
            {
              "description": "Çarpılmış Hesaplanan Kurum Teminat",
              "value": "0"
            },
            {
              "description": "Kısa Opt.Asgari Teminatı",
              "value": "0"
            },
            {
              "description": "Kullanılabilir Teminat",
              "value": "0"
            },
            {
              "description": "Bekleyen ödenecek prim",
              "value": "0"
            }
          ]
        }
      };
    },
  });
}

export function ViopProfitLoss() {
  return useQuery({
    queryFn: () => {
      return {
        "data": {
          "profitLossGroup": {
            "atPresentOpenContractsEndOfPeriod": [
              {
                "customerId": "0000-0009QA-CUS",
                "accountId": "0000-00C6AS-ACC",
                "financialInstitutionId": "0000-003KNJ-FIN",
                "code": "F_AEFES0822",
                "unitNominal": "100",
                "shotLong": "KISA",
                "maturityDate": "31.08.2022 00:00:00",
                "closingPrice": "41.3200",
                "cost": "0.0000",
                "openUnit": "1",
                "profitLoss": "-4132.0000"
              },
              {
                "customerId": "0000-0009QA-CUS",
                "accountId": "0000-00C6AS-ACC",
                "financialInstitutionId": "0000-003LNJ-FIN",
                "code": "F_AEFES0922",
                "unitNominal": "100",
                "shotLong": "UZUN",
                "maturityDate": "30.09.2022 00:00:00",
                "closingPrice": "40.0600",
                "cost": "0.0000",
                "openUnit": "1",
                "profitLoss": "4006.0000"
              }
            ],
            "totalRecordForVirtualBranch": {
              "grossProfitLoss": "-269815753.6247",
              "interestCustody": "0.0000",
              "commissionExpense": "0.0000",
              "netProfitLoss": "-269815753.6247"
            }
          }
        }
      };
    },
  });
}

export function ViopCustomerTransactions() {
  return useQuery({
    queryFn: () => {
      return {
        "data": {
          "customerTransactionsGroupList": [
            {
              "contract": "F_XU0300423",
              "shortLong": "Uzun",
              "units": "1",
              "leftUnits": "0",
              "price": "5500.0000",
              "transactionId": "19U-0M2LO",
              "sessionType": "1",
              "sessionTypeDesc": "Normal",
              "transactionDate": "10.04.2023 00:00:00",
              "transactionEndDate": "10.04.2023 00:00:00",
              "transactionEndDateType": "DAY",
              "transactionUnitNominal": "10.000000",
              "transactionUnit": "1",
              "transactionExId": "19U-0M2LO",
              "description": "Emir numara almadan iptal edilmiş.",
              "orderTime": "10.04.2023 10:01:47",
              "validity": "Günlük",
              "financialInstitutionId": "0000-003POH-FIN",
              "orderType": "KPY",
              "priceType": "Limitli",
              "priceTypeName": "LMT",
              "info": "Emir numara almadan iptal edilmiş.",
              "priceCondition": "0.0000",
              "realizedUnits": "0",
              "priceAverage": "0",
              "transStatusName": "CANCEL_ORDER",
              "triggerPriceTypeCode": "",
              "triggerPriceType": "",
              "triggerFinancialInstitutionName": "",
              "timeInForce": "DAY",
              "ordType": "LMT",
              "lastFillTime": "01.01.0001 00:00:00",
              "commission": "0"
            }
          ]
        }
      };
    },
  });
}

export function TradeConfirmRecords() {
  return useQuery({
    queryFn: () => {
      return {
        "data": {
          "tradeConfirmRecordDetails": [
            {
              "fullName": "Birey-000OKU",
              "customerId": "0000-0009QA-CUS",
              "customerExtId": "7012445",
              "accountId": "0000-00C6AS-ACC",
              "accountExtId": "7012445-101",
              "accountId1": "",
              "finInstId": "",
              "fillDate": "1/23/2023 12:00:00 AM",
              "market": "VIOP",
              "longShort": "KISA",
              "finInstName": "F_AKBNK0123",
              "currency": "TL",
              "maturityDate": "31.01.2023 00:00:00",
              "usagePrice": "0",
              "optionBonus": "0",
              "fillPrice": "19.720000",
              "fillUnits": "1",
              "orderType": "KPY",
              "fillSession": "1",
              "collateral": "0",
              "commission": "0.490000",
              "contractSubgroup": "",
              "divExtId": "858",
              "divName": "AKDENİZ ŞUBESİ"
            },
            {
              "fullName": "Birey-000OKU",
              "customerId": "0000-0009QA-CUS",
              "customerExtId": "7012445",
              "accountId": "0000-00C6AS-ACC",
              "accountExtId": "7012445-101",
              "accountId1": "",
              "finInstId": "",
              "fillDate": "1/23/2023 12:00:00 AM",
              "market": "VIOP",
              "longShort": "KISA",
              "finInstName": "F_AKBNK0123",
              "currency": "TL",
              "maturityDate": "31.01.2023 00:00:00",
              "usagePrice": "0",
              "optionBonus": "0",
              "fillPrice": "19.720000",
              "fillUnits": "1",
              "orderType": "KPY",
              "fillSession": "1",
              "collateral": "0",
              "commission": "0.490000",
              "contractSubgroup": "",
              "divExtId": "858",
              "divName": "AKDENİZ ŞUBESİ"
            }
          ]
        }
      };
    },
  });
}

export function ViopSymbolList() {
  return useQuery({
    queryFn: () => {
      return {
        "data": {
          "symbolList": [
            {
              "symbol": "O_AKBNKE0523P15.00",
              "financialInstitutionId": "0000-003TU0-FIN",
              "fixPartyId": "1",
              "derivativesLevel": "1"
            },
            {
              "symbol": "O_AKBNKE0523P15.50",
              "financialInstitutionId": "0000-003TU1-FIN",
              "fixPartyId": "1",
              "derivativesLevel": "1"
            }
          ]
        }
      };
    },
  });
}
import { useQuery } from '@tanstack/react-query';

export function GetPhoto() {
    return useQuery({
        queryFn: () => {
            return {
                "data": {
                    "errorCode": "00000",
                    "errorMessage": "",
                    "customerPhoto": "/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAoHBwgHBgoICAgLCgoLDhgQDg0NDh0VFhEYIx8lJCIfIiEmKzcvJik0KSEiMEExNDk7Pj4+JS5ESUM8SDc9Pjv/2wBDAQoLCw4NDhwQ/HFKxutT/2Q=="
                }
            };
        },
    });
}

export function GetCustomerInformation() {
    return useQuery({
        queryFn: () => {
            return {
                "data": {
                    "name": "UTKU",
                    "surname": "ERKAL"
                }
            };
        },
    });
}

export function GetWatchlist() {
    return useQuery({
        queryFn: () => {
            return {
                "data": {
                    "userRefText": "000000000003554",
                    "userWatchLists": [
                        {
                            "listName": "My first list",
                            "symbolList": [
                                "GARAN",
                                "KOZOL"
                            ]
                        },
                        {
                            "listName": "Other list",
                            "symbolList": [
                                "AEFES",
                                "ENJSA"
                            ]
                        }
                    ]
                }
            };
        },
    });
}

export function CreateWatchlist() {
    return useQuery({
        queryFn: () => {
            return {
                "data": {
                    "isCreated": true,
                    "userRefText": "000000000003554"
                }
            };
        },
    });
}

export function UpdateWatchlist() {
    return useQuery({
        queryFn: () => {
            return {
                "data": {
                    "modifiedUserCount": 1,
                    "userRefText": "000000000003554"
                }
            };
        },
    });
}

export function ListResearchReports() {
    return useQuery({
        queryFn: () => {
            return {
                "data": {
                    "items": [
                        {
                            "title": "Piyasalarda Bugün ve Yarın",
                            "shortDescription": "Türkiye varlıkları dünya fiyatlamaları ile aradaki farkı kapatmaya devam ediyor.",
                            "categoryName": "Şirket ve Sektör Raporları",
                            "publishDate": "01.10.2020 00:00",
                            "pdfUrl": "https://test.garantibbvayatirim.com.tr/medium/ResearchReports-Constant-996.vsf",
                            "fileType": "pdf"
                        }
                    ],
                    "totalItems": 1
                }
            };
        },
    });
}

export function ListReportCategories() {
    return useQuery({
        queryFn: () => {
            return {
                "data": {
                    "researchReportCategories": [
                        {
                            "id": "560",
                            "name": "Günlük Bültenler",
                            "researchReportsCount": 0,
                            "childCategories": [
                                {
                                    "id": "580",
                                    "name": "Gün Ortası Notları",
                                    "researchReportsCount": 11
                                },
                                {
                                    "id": "578",
                                    "name": "Piyasalara Bakış",
                                    "researchReportsCount": 31
                                },
                                {
                                    "id": "574",
                                    "name": "Veri Akışı",
                                    "researchReportsCount": 0
                                },
                                {
                                    "id": "576",
                                    "name": "Güne Başlarken",
                                    "researchReportsCount": 3
                                }
                            ]
                        },
                        {
                            "id": "1060",
                            "name": "Uluslararası Piyasalar",
                            "researchReportsCount": 0,
                            "childCategories": [
                                {
                                    "id": "1076",
                                    "name": "Trade Önerisi",
                                    "researchReportsCount": 0
                                },
                                {
                                    "id": "1074",
                                    "name": "Makro Ekonomi",
                                    "researchReportsCount": 0
                                },
                                {
                                    "id": "1072",
                                    "name": "Avrupa Ekonomi",
                                    "researchReportsCount": 1
                                },
                                {
                                    "id": "1070",
                                    "name": "Gündem",
                                    "researchReportsCount": 1
                                },
                                {
                                    "id": "1068",
                                    "name": "Tahvil Strateji",
                                    "researchReportsCount": 0
                                },
                                {
                                    "id": "1066",
                                    "name": "Döviz Strateji",
                                    "researchReportsCount": 0
                                },
                                {
                                    "id": "1064",
                                    "name": "Emtia Strateji",
                                    "researchReportsCount": 0
                                },
                                {
                                    "id": "1062",
                                    "name": "Hisse Strateji",
                                    "researchReportsCount": 0
                                }
                            ]
                        },
                        {
                            "id": "556",
                            "name": "Tematik Raporlar",
                            "researchReportsCount": 0,
                            "childCategories": [
                                {
                                    "id": "1058",
                                    "name": "Paranın Rengi",
                                    "researchReportsCount": 1
                                },
                                {
                                    "id": "572",
                                    "name": "Pusula",
                                    "researchReportsCount": 0
                                },
                                {
                                    "id": "570",
                                    "name": "Sektörel Görünüm",
                                    "researchReportsCount": 1
                                },
                                {
                                    "id": "558",
                                    "name": "Glokal Teknik Analiz",
                                    "researchReportsCount": 3
                                }
                            ]
                        },
                        {
                            "id": "554",
                            "name": "Şirket ve Sektör Raporları",
                            "researchReportsCount": 1,
                            "childCategories": [
                                {
                                    "id": "568",
                                    "name": "Strateji Raporları",
                                    "researchReportsCount": 1
                                },
                                {
                                    "id": "566",
                                    "name": "Sektör Raporları",
                                    "researchReportsCount": 0
                                },
                                {
                                    "id": "564",
                                    "name": "Şirket Raporları",
                                    "researchReportsCount": 1
                                },
                                {
                                    "id": "562",
                                    "name": "Finansal Analiz",
                                    "researchReportsCount": 2
                                }
                            ]
                        },
                        {
                            "id": "552",
                            "name": "Model Portföy",
                            "researchReportsCount": 0,
                            "childCategories": []
                        },
                        {
                            "id": "550",
                            "name": "Makro Ekonomi Raporları",
                            "researchReportsCount": 0,
                            "childCategories": []
                        }
                    ]
                }
            };
        },
    });
}

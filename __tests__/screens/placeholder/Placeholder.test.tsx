import React, { ReactNode } from "react";
import { PlaceholderHomeScreen } from "../../../src/screens/Placeholder";
import { QueryClient, QueryClientProvider, useQuery } from "@tanstack/react-query";
import { render, fireEvent } from "@testing-library/react-native";
import { ReportBackProvider, rnBoilerplateStore } from "../../../src/context";
import { renderHook } from "@testing-library/react-hooks";
import { GetPortfolioStocks } from "../../api/customer";

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      retry: false,
    },
  },
});

const wrapper = ({ children }: { children: ReactNode }) => (
  <QueryClientProvider client={queryClient}>{children}</QueryClientProvider>
);

export function useCustomHook() {
  return useQuery({ queryKey: ["customHook"], queryFn: () => "Hello" });
}

describe("placeholder testing", () => {
  afterEach(() => {
    queryClient.clear();
  });

  rnBoilerplateStore.setUser({ name: "etr" });

  it("user", () => {
    const { getByText } = render(
      <ReportBackProvider>
        <PlaceholderHomeScreen />
      </ReportBackProvider>,
      { wrapper }
    );
    const name = getByText("etr");
    expect(name).toBeDefined();
  });

  it("placeholder text", () => {
    const { getByText } = render(
      <ReportBackProvider>
        <PlaceholderHomeScreen />
      </ReportBackProvider>,
      { wrapper }
    );
    const text = getByText("placeholder home");
    expect(text).toBeDefined();
  });

  it("GetPortfolioStocks", async () => {
    const { result, waitFor } = renderHook(() => GetPortfolioStocks(), {
      wrapper,
    });
    await waitFor(() => result.current.isSuccess);

    const {
      current: { data, isSuccess, isLoading },
    } = result;
    expect(data?.data).toEqual(
      expect.objectContaining({
        className: expect.any(String),
        timestamp: expect.any(String),
        instantPosSum: expect.any(Object),
      })
    );
    expect(isSuccess).toEqual(true);
  });
});

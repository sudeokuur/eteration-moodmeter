# SETUP

Codepush güncellemeleri AppCenter Distribute üzerinden yönetilmektedir. AppCenter CodePush dokümanlarına Microsoft [https://docs.microsoft.com/en-us/appcenter/distribution/codepush](https://docs.microsoft.com/en-us/appcenter/distribution/codepush) sitesinden erişebilirsiniz.  Android ve IOS için ayrı paketler vardır, her uygulama altında iki tane farklı dağtım grubu vardır: Production ve Staging.  


- Android: [AppCenter Android Paketi](https://appcenter.ms/orgs/eteration-gtrader/apps/gtrader-android/distribute/code-push)
    - [Production](https://appcenter.ms/orgs/eteration-gtrader/apps/gtrader-android/distribute/code-push/Production)
    - [Staging](https://appcenter.ms/orgs/eteration-gtrader/apps/gtrader-android/distribute/code-push/Staging)

- iOS: [AppCenter iOS Paketi](https://appcenter.ms/orgs/eteration-gtrader/apps/gtrader-ios/distribute/code-push)
    - [Production](https://appcenter.ms/orgs/eteration-gtrader/apps/gtrader-ios/distribute/code-push/Production)
    - [Staging](https://appcenter.ms/orgs/eteration-gtrader/apps/gtrader-ios/distribute/code-push/Staging)


Bu dağıtımları izlemek, yeni dağtım eklemek, yeni güncelleme release'i çıkmak ve benzeri işlmeler için için AppCnetr koncol ayada AppCenter CLI kullanılabilir.

```shell

        npm install -g appcenter-cli@2.11.0  #(MUST BE v2.11.0 or above)
        appcenter login  # Eteration hesabı il elogin olunmalıdır

        # Deployment Listesi ve CODEPUSH_KEY 
        appcenter codepush release-react -a eteration-gtrader/gtrader-android -d Staging
        appcenter codepush deployment list --app eteration-gtrader/gtrader-android  -k

        # Paketin İmzalanması için Public/Private Key oluşturma (Bu sadece bir kere yapılmadılır)
        # Lütfen daha önceden oluşturulan pem doslarını ezmeyiniz (keys/codepush.pem)
        # openssl genrsa -out keys/codepush.pem
        # openssl rsa -pubout -in keys/codepush.pem -out keys/codepush-public.pem


        # Yeni IOS Staging Paketi çıkmak için (#Release ve Debug konfigleri test backend'e gider)
        appcenter codepush release-react -a eteration-gtrader/gtrader-ios --private-key-path codepush/private.pem --plist-file ios/rnboilerplate/Info.plist -c Staging  -d Staging -m


        # Yeni Android Staging Paketi çıkmak için
         appcenter codepush release-react -a eteration-gtrader/gtrader-android --private-key-path codepush/private.pem -c Staging  -d Staging -m 
      

```

> Description Format --> --description "HotFix-yyyyMMddHHmm"

# Flags

- -m --mandatory
- -x --disabled

More: https://docs.microsoft.com/en-us/appcenter/distribution/codepush/cli
# Standarts

Created by: Hakan DURSUN
Created time: April 10, 2023 11:03 AM
Last edited by: Hakan DURSUN
Last edited time: April 14, 2023 9:04 AM
Tags: Standart

# İsimlendirme standartları

> **Dosya isimleri** camelCase yazılır : components,screens
> 
> 
> **Component isimleri** PascalCase şeklinde yazılır : PrimaryButton, CurrencyView
> 
> Her dosyanın içerisindekiler index.js ile dışa aktarılacaktır.
> 
> **Değişkenler** camelCase.
> 

## Dosya Yapısı

Aşağıda bulunan dosya yapısı standart bir projedir ve proje içerisindeki **__test__** dosya ****yapısı ile tamamen aynıdır

- src
    - api
    - components
        
        business
        
        ui
        
    - context
        - axios
        
        NameContext
        
        NameProvider
        
        NameStore
        
        index
        
    - routes
        
        AppStack
        
        AuthStack
        
        index
        
        OnboardingStack
        
    - screens
    - themes
        - assets
        - components
        - svgs
        
        index
        
        schemes
        
    - translations
        
        index
        
    - utils
        
        …all tools
        
    
    App.tsx
    

## İmport sıralaması

1. React
2. Kütüphaneler (Alfabetik sırayla)
3. Projeden içe aktarımlar (Alfabetik sırayla)
4. Diğer importlar

# Prettier ayarları

- arrowParens: 'avoid': ok işaretleri için parantezlerden kaçının.
- bracketSpacing: true: nesnelerdeki ve dizilerdeki süslü parantezlerin etrafındaki boşluğu koruyun.
- printWidth: 120: satır başına kaç karakter sığdırılacağı.
- tabWidth: 2: girinti yapmak için kaç boşluk kullanılacağı.
- semi: true: satır sonundaki noktalı virgüllerin kullanılıp kullanılmayacağı.
- singleQuote: true: tek tırnak kullanın.
- trailingComma: 'all': son öğeden sonra her zaman bir virgül koyun.
- bracketSameLine: true: nesnelerdeki süslü parantezleri satırın sonuna koymayın.
- jsxBracketSameLine: true: JSX öğelerindeki süslü parantezleri satırın sonuna koymayın.

## Eslint ayarları

- Parser olarak **'@typescript-eslint/parser’**
- Klavız olarak **'airbnb', 'airbnb/hooks']** (extends edilerek)
- overrides : *.ts ve *.tsx dosyaları için geçerli olacak
- …ve projeye bağlı diğer kurallar.
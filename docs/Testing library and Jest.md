# Testing library and Jest

Created by: Hakan DURSUN
Created time: April 10, 2023 11:03 AM
Last edited by: Hakan DURSUN
Last edited time: April 14, 2023 9:30 AM
Tags: Testing

<aside>
💡 **Tip: React ve React-native için test yapılımı**

</aside>

## Kaynaklar

> [*Testing/library*](https://testing-library.com/docs/react-native-testing-library/example-intro)
> 
> 
> [*React Native test örnekleri*](https://reactnativetesting.io/component/testing)
> 
> [*Jest*](https://jestjs.io/docs/asynchronous)
> 
> [*Axios mock*](https://www.npmjs.com/package/jest-mock-axios)
> 

## Terimler

- In english, “mock” can mean “not real”. Yani gerçek olmayan

# 👀 Paketler ve içerikleri

> @testing-library/react-native
> 
> 
> @testing-library/user-event
> 
> jest
> 

---

# 💭 Paket içeriklerinin açıklamaları

**@testing-library/react-native kütüphanesinden**

> render ⇒ işlem yapılacak sayfanın render edilmesi için fonksiyon
> 
> 
> screen ⇒ render edilen sayfa için class, içerisinden fonksiyonları çekerek kullan.
> 
> waitFor ⇒ eğer asyn çalışan ve ya time işlemi olan vb. durumlar için kullan
> 

**@testing-library/user-event kütüphanesinden**

> 
> 

---

# 🛫 Fonksiyonlar açıklamaları

> **fireEvent(node: HTMLElement, event: Event)**
> 
> 
> -await screen.findByText(”text”,queryOptions,waitForOptions) findBy.. ile çağrılanların içerisi    default olarak böyledir.
> 

### Genel tanımlar

> **getBy** ⇒ ör:getByRole,getByText gibi çağrılmalarda kullanılır. Eşleşmeme durumunda hata fırlatır birden çok eşleşmede hata fıraltır
> 
> 
> **getAllBy** ⇒ birden çok eşleşmede elementi dönderir
> 
> Aynı durum All ve normallerdede geçerli
> 
> **queryBy** ⇒ ör:queryByDisplayValue, queryByTitle ,eşleşmeme durumunda null döner
> 
> **queryAllBy**
> 
> **findBy** ⇒ getBy la aynı çalışır
> 

***EXPECT(**).screen* den sonra şart için ()**

[https://testing-library.com/docs/queries/about](https://testing-library.com/docs/queries/about)

> **toHaveLength(n)** ⇒ n uzunluğunda mı ?
> 
> 
> **findByRole(”role”,{name: /name/i})** ⇒ rolü bu olanın name’i buysa 
> 
> **getByRole(”role”)** ⇒ rolü ile bul
> 
> **getAllByRole(”role”)** ⇒  bu role ait olan hepsini bul
> 

### Jest’e ait olanlar

> **toEqual(n)** ⇒ eşit mi ?
> 
> 
> **toContain(object)** ⇒ içeriyor mu ?
> 
> **toThrow()** ⇒ fonksiyon çalıştığında hata verdi mi ?
> 
> **toHaveBeenCalled(x)** ⇒ x kere çağrıldı mı ?
> 
> **toBeInTheDocument()** ⇒ Dokümanın içerisinde mi ?
> 

# Standartlar

> **render**(<Component />)
> 
> 
> Manipüle et yada elementleri bul.
> 
> Component yapıyor mu, dediğimiz işlemleri yaz
> 

## ReportBack için test yapısı

```jsx
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import { render, screen } from "@testing-library/react-native"
import axios from "axios";
import React, { ReactNode } from "react";
import { INSTANT_POS_SUMMARY } from "../../../src/api/paths";
import { ReportBackProvider, rnBoilerplateStore } from "../../../src/context";
import { DashboardScreen } from "../../../src/screens/Home";
import axiosMock from "jest-mock-axios"

const queryClient = new QueryClient({
    defaultOptions: {
        queries: {
            retry: false,
        },
    },
});

const wrapper = ({ children }: { children: ReactNode }) => (
    <QueryClientProvider client={queryClient}>{children}</QueryClientProvider>
);

jest.mock('@react-navigation/native', () => {
  return {
    useNavigation: () => ({
      navigate: jest.fn(),
    }),
    useTheme:()=>({
        colors:{
            text:"#fff",
						//Gerekli olması halinde kendimiz gireceğiz
        }
    })
  };
});

test("home dashborad render test", async () => {
    axiosMock.get.mockRejectedValue(axiosİçinData);
    rnBoilerplateStore.setUser({name:"name"})
	   render(
        <ReportBackProvider>
            <DashboardScreen />
        </ReportBackProvider>,
        { wrapper },
    );
    
    const stocks = await axios.post(INSTANT_POS_SUMMARY, {
        login: {
            accountNumber: '3676410',
            chosenSubAccount: '3676410-100',
            hcpClientType: '',
        },
        type: '1',
    });

})
```
let mockFetchResponses = {};

global.setMockFetchResponse = (url, response) => {
  mockFetchResponses[url] = response;
};

global.fetch = jest.fn(url => {
  return Promise.resolve({
    json: () => Promise.resolve(mockFetchResponses[url]),
  });
});

jest.mock('react-native-reanimated', () => {
  const Reanimated = require('react-native-reanimated/mock');
  Reanimated.default.call = () => { };

  return Reanimated;
});
jest.mock('react-native/Libraries/Animated/NativeAnimatedHelper');
jest.mock('react-native-gesture-handler', () => { });

jest.mock('@react-native-async-storage/async-storage', () =>
  require('@react-native-async-storage/async-storage/jest/async-storage-mock'),
);

jest.mock('@notifee/react-native', () =>
  require('@notifee/react-native/jest-mock'),
);

jest.mock('react-native/Libraries/EventEmitter/NativeEventEmitter');

jest.mock('@react-native-firebase/messaging', () => () => ({
  deleteToken: jest.fn(),
  hasPermission: jest.fn(() => Promise.resolve(true)),
  subscribeToTopic: jest.fn(),
  unsubscribeFromTopic: jest.fn(),
  requestPermission: jest.fn(() => Promise.resolve(true)),
  setBackgroundMessageHandler: jest.fn(),
  registerForRemoteNotifications: jest.fn(),
  getToken: jest.fn(() => Promise.resolve('myMockToken')),
  getAPNSToken: jest.fn(() => Promise.resolve('myMockToken')),
  onTokenRefresh: jest.fn(() => Promise.resolve('myMockToken')),
  onMessage: () => jest.fn(),
  isRegisteredForRemoteNotifications: () => false,
  isAutoInitEnabled: () => false,
}));
jest.mock('react-native-code-push', () => {
  const cp = () => app => app;
  Object.assign(cp, {
    InstallMode: {},
    CheckFrequency: {},
    SyncStatus: {},
    UpdateState: {},
    DeploymentStatus: {},
    DEFAULT_UPDATE_DIALOG: {},

    allowRestart: jest.fn(),
    checkForUpdate: jest.fn(() => Promise.resolve(null)),
    disallowRestart: jest.fn(),
    getCurrentPackage: jest.fn(() => Promise.resolve(null)),
    getUpdateMetadata: jest.fn(() => Promise.resolve(null)),
    notifyAppReady: jest.fn(() => Promise.resolve()),
    restartApp: jest.fn(),
    sync: jest.fn(() => Promise.resolve(1)),
    clearUpdates: jest.fn(),
  });
  return cp;
});

// const PropTypes = require('prop-types');

// PropTypes.shape = () => {};
// PropTypes.arrayOf = () => {};
// PropTypes.oneOfType = () => {};
// PropTypes.objectOf = () => {};
// PropTypes.string = () => {};
// PropTypes.func = () => {};
// PropTypes.number = () => {};
// PropTypes.bool = () => {};

// module.exports = PropTypes;

jest.mock('react-native-snap-carousel', () => ({
  __esModule: true,
  default: 'Carousel',
  Pagination: 'Pagination',
}));

jest.mock('react-i18next', () => ({
  useTranslation: () => ({ t: key => key }),
}));

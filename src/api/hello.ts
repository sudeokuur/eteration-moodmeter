import { useQuery } from '@tanstack/react-query';
import { apiClient } from '../context';

export function Foo() {
  return useQuery(['foo'], {
    queryFn: () => !!apiClient && apiClient.get('/api/foo'),
    staleTime: 1000,
    cacheTime: 1500,
    retry: 0,
    enabled: true,
  });
}

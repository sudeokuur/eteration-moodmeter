import React from 'react';
import { View, ActivityIndicator } from 'react-native';
import { useColors } from '../hooks/useColors';

export function Loading() {
  const { colors } = useColors();

  return (
    <View
      style={{
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <ActivityIndicator color={colors.action} animating size="large" />
    </View>
  );
}

import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

const SvgArrow = {
  svg: (
    <Svg width="24" height="24" fill="none">
      <Path d="M15.707.293a1 1 0 1 0-1.414 1.414L16.586 4H4a1 1 0 1 0 0 2h15a1 1 0 0 0 .707-1.707l-4-4ZM1 8a1 1 0 0 0-.707 1.707l4 4a1 1 0 0 0 1.414-1.414L3.414 10H16a1 1 0 1 0 0-2H1Z" />
    </Svg>
  ),
  viewBox: '0 0 20 14',
};

export default SvgArrow;

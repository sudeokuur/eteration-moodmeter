import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

const SvgCross = {
  svg: (
    <Svg width="24" height="24" fill="none">
      <Path
        d="M13.707 1.707A1 1 0 0 0 12.293.293L7 5.586 1.707.293A1 1 0 0 0 .293 1.707L5.586 7 .293 12.293a1 1 0 1 0 1.414 1.414L7 8.414l5.293 5.293a1 1 0 0 0 1.414-1.414L8.414 7l5.293-5.293Z"
        fillOpacity={0.64}
      />
    </Svg>
  ),
  viewBox: '0 0 14 14',
};
export default SvgCross;

import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

const SvgDocument = {
  svg: (
    <Svg width="24" height="24" fill="none">
      <Path d="M4 8a1 1 0 0 1 1-1h2a1 1 0 0 1 0 2H5a1 1 0 0 1-1-1ZM5 11a1 1 0 1 0 0 2h4a1 1 0 1 0 0-2H5Z" />
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M9.664.253A1 1 0 0 0 9 0H3a3 3 0 0 0-3 3v12a3 3 0 0 0 3 3h8a3 3 0 0 0 3-3V4.556a1 1 0 0 0-.336-.748l-4-3.555ZM12 5.556h-1a3 3 0 0 1-3-3V2H3a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V5.556Z"
      />
    </Svg>
  ),
  viewBox: '0 0 14 18',
};

export default SvgDocument;

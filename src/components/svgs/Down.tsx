import * as React from 'react';
import Svg, { Path } from 'react-native-svg';
import { Colors } from 'react-native-ui-lib';

const SvgDown = {
  svg: (
    <Svg width="24" height="24" fill="none">
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M7.047 0H.953C.427 0 0 .44 0 .984c0 .185.05.365.145.522l3.047 5.031a.936.936 0 0 0 1.616 0l3.047-5.031A1.003 1.003 0 0 0 7.552.15.931.931 0 0 0 7.047 0Z"
        fill={Colors.redPrice}
      />
    </Svg>
  ),
  viewBox: '0 0 8 7',
};
export default SvgDown;

import * as React from 'react';
import Svg, { SvgProps, Path } from 'react-native-svg';
const SvgDownArrow = (props: SvgProps) => (
  <Svg width="24" height="24" viewBox="0 0 10 6" fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
    <Path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M9.707.293a1 1 0 0 1 0 1.414l-4 4a1 1 0 0 1-1.414 0l-4-4A1 1 0 0 1 1.707.293L5 3.586 8.293.293a1 1 0 0 1 1.414 0Z"
      fill="#2DCCCD"
    />
  </Svg>
);
export default SvgDownArrow;

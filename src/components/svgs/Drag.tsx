import * as React from 'react';
import Svg, { SvgProps, Path } from 'react-native-svg';
const SvgDrag = (props: SvgProps) => (
  <Svg width="24" height="24" viewBox="0 0 20 10" fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
    <Path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M0 1a1 1 0 0 1 1-1h18a1 1 0 1 1 0 2H1a1 1 0 0 1-1-1Zm0 4a1 1 0 0 1 1-1h18a1 1 0 1 1 0 2H1a1 1 0 0 1-1-1Zm0 4a1 1 0 0 1 1-1h18a1 1 0 1 1 0 2H1a1 1 0 0 1-1-1Z"
      fill={props.fill}
      fillOpacity={0.3}
    />
  </Svg>
);
export default SvgDrag;

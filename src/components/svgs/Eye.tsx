import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

const SvgEye = {
  svg: (
    <Svg width="24" height="24" fill="none">
      <Path d="M13 9a2 2 0 1 1-4 0 2 2 0 0 1 4 0Z" />
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M6 9a5 5 0 1 1 10 0A5 5 0 0 1 6 9Zm5-3a3 3 0 1 0 0 6 3 3 0 0 0 0-6Z"
      />
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M21.938 8.654C18.117-1.718 3.883-1.718.062 8.654a1 1 0 0 0 .016.734c2.15 5.106 6.504 7.737 10.922 7.737 4.418 0 8.772-2.631 10.922-7.737a1 1 0 0 0 .016-.734ZM11 15.125c-3.481 0-7.024-2.002-8.918-6.148C5.406.84 16.594.84 19.918 8.977c-1.895 4.146-5.437 6.148-8.918 6.148Z"
      />
    </Svg>
  ),
  viewBox: '0 0 22 18',
};
export default SvgEye;

import * as React from 'react';
import Svg, { Circle } from 'react-native-svg';

const SvgFrame = {
  svg: (
    <Svg width="24" height="24" fill="none">
      <Circle cx={20} cy={20} r={18} stroke="#fff" strokeOpacity={0.08} strokeWidth={4} />
    </Svg>
  ),
  viewBox: '0 0 40 40',
};

export default SvgFrame;

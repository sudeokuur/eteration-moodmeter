import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

const SvgGreySquare = {
  svg: (
    <Svg width="8" height="8" fill="none">
      <Path fill="#ACAEB9"  d="M0.9999 0h6s1 0 1 1v6s0 1 -1 1h-6s-1 0 -1 -1v-6s0 -1 1 -1" />
    </Svg>
  ),
  viewBox: '0 0 8 8',
};
export default SvgGreySquare;

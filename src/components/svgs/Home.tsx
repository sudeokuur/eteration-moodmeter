import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

const SvgHome = {
  svg: (
    <Svg width="24" height="24" fill="none">
      <Path d="M10.427 2.622a1 1 0 0 1 1.146 0l8.854 6.197a1 1 0 0 0 1.146-1.638L12.72.984a3 3 0 0 0-3.44 0L.427 7.18a1 1 0 0 0 1.146 1.638l8.854-6.197Z" />
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M19 19v-7.892a2 2 0 0 0-.94-1.696l-6-3.75a2 2 0 0 0-2.12 0l-6 3.75A2 2 0 0 0 3 11.108V19a2 2 0 0 0 2 2h4a2 2 0 0 0 2-2v-6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1v6a2 2 0 0 0 4 0ZM5 13a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1v1a1 1 0 0 1-1 1H6a1 1 0 0 1-1-1v-1Z"
      />
    </Svg>
  ),
  viewBox: '0 0 22 21',
};

export default SvgHome;

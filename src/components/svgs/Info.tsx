import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

const SvgInfo = {
  svg: (
    <Svg width="24" height="24" fill="none">
      <Path d="M7 14A7 7 0 1 0 7 0a7 7 0 0 0 0 14ZM8 4a1 1 0 1 1-2 0 1 1 0 0 1 2 0ZM6 7a1 1 0 0 1 2 0v3a1 1 0 1 1-2 0V7Z" />
    </Svg>
  ),
  viewBox: '0 0 14 14',
};

export default SvgInfo;

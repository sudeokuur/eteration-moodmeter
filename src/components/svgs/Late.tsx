import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

const SvgLate = {
  svg: (
    <Svg width="24" height="24" fill="none">
      <Path d="M7.5 3.5a.5.5 0 0 0-1 0V7a.5.5 0 0 0 .5.5h3.5a.5.5 0 0 0 0-1h-3v-3Z" />
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M14 7A7 7 0 1 1 0 7a7 7 0 0 1 14 0Zm-1 0A6 6 0 1 1 1 7a6 6 0 0 1 12 0Z"
      />
    </Svg>
  ),
  viewBox: '0 0 14 14',
};
export default SvgLate;

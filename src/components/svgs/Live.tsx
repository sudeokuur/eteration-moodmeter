import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

const SvgLive = {
  svg: (
    <Svg width="24" height="24" fill="none">
      <Path
        d="M4.464 2.465A4.984 4.984 0 0 0 3 6c0 1.38.56 2.63 1.464 3.535M3 1.1A6.977 6.977 0 0 0 1 6c0 1.907.763 3.636 2 4.899M11.57 2.5A4.984 4.984 0 0 1 13 6a4.984 4.984 0 0 1-1.43 3.5M13 1.101A6.977 6.977 0 0 1 15 6a6.977 6.977 0 0 1-2 4.899M10 6a2 2 0 1 1-4 0 2 2 0 0 1 4 0Z"
        strokeLinecap="round"
      />
    </Svg>
  ),
  viewBox: '0 0 16 12',
};
export default SvgLive;

import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

const SvgMagnifier = {
  svg: (
    <Svg width="24" height="24" fill="none">
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M8.5 0a8.5 8.5 0 1 0 5.262 15.176l3.53 3.531a1 1 0 0 0 1.415-1.414l-3.531-3.531A8.5 8.5 0 0 0 8.5 0ZM2 8.5a6.5 6.5 0 1 1 13 0 6.5 6.5 0 0 1-13 0Z"
      />
    </Svg>
  ),
  viewBox: '0 0 19 19',
};

export default SvgMagnifier;

import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

const SvgMarkets = {
  svg: (
    <Svg width="24" height="24" fill="none">
      <Path d="M17.753.342a1 1 0 0 1-.095 1.41l-8 7A1 1 0 0 1 8.4 8.8L5 6.25 1.6 8.8A1 1 0 0 1 .4 7.2l4-3a1 1 0 0 1 1.2 0l3.352 2.514 7.39-6.467a1 1 0 0 1 1.41.095ZM18 7a1 1 0 1 0-2 0v10a1 1 0 1 0 2 0V7ZM13 11a1 1 0 0 1 1 1v5a1 1 0 1 1-2 0v-5a1 1 0 0 1 1-1ZM2 14a1 1 0 1 0-2 0v3a1 1 0 1 0 2 0v-3ZM10 14a1 1 0 1 0-2 0v3a1 1 0 1 0 2 0v-3Z" />
      <Path d="M6 10a1 1 0 0 0-2 0v7a1 1 0 1 0 2 0v-7Z" />
    </Svg>
  ),
  viewBox: '0 0 18 18',
};

export default SvgMarkets;

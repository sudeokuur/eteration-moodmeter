import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

const SvgMinus = {
  svg: (
    <Svg width="24" height="24">
      <Path fillRule="evenodd" clipRule="evenodd" d="M0 1a1 1 0 0 1 1-1h10a1 1 0 1 1 0 2H1a1 1 0 0 1-1-1Z" />
    </Svg>
  ),
  viewBox: '0 0 12 2',
};
export default SvgMinus;

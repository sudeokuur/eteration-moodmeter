import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

const SvgOrders = {
  svg: (
    <Svg width="24" height="24" fill="none">
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M0 2a2 2 0 0 1 2-2h2a2 2 0 0 1 2 2v2a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2Zm2 0h2v2H2V2ZM0 10a2 2 0 0 1 2-2h2a2 2 0 0 1 2 2v2a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2v-2Zm2 0h2v2H2v-2Z"
      />
      <Path d="M8 1a1 1 0 0 1 1-1h10a1 1 0 1 1 0 2H9a1 1 0 0 1-1-1ZM9 4a1 1 0 1 0 0 2h7a1 1 0 1 0 0-2H9ZM8 9a1 1 0 0 1 1-1h9a1 1 0 1 1 0 2H9a1 1 0 0 1-1-1ZM9 12a1 1 0 1 0 0 2h6a1 1 0 1 0 0-2H9Z" />
    </Svg>
  ),
  viewBox: '0 0 20 14',
};

export default SvgOrders;

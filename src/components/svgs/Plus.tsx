import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

const SvgPlus = {
  svg: (
    <Svg width="24" height="24">
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M6 0a1 1 0 0 1 1 1v4h4a1 1 0 1 1 0 2H7v4a1 1 0 1 1-2 0V7H1a1 1 0 0 1 0-2h4V1a1 1 0 0 1 1-1Z"
      />
    </Svg>
  ),
  viewBox: '0 0 12 12',
};

export default SvgPlus;

import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

const SvgPortfolio = {
  svg: (
    <Svg width="24" height="24" fill="none">
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M8 2a1 1 0 0 0-1 1h6a1 1 0 0 0-1-1H8Zm7 1a3 3 0 0 0-3-3H8a3 3 0 0 0-3 3H3a3 3 0 0 0-3 3v8a3 3 0 0 0 3 3h14a3 3 0 0 0 3-3V6a3 3 0 0 0-3-3h-2ZM3 5a1 1 0 0 0-1 1v2.198l3 .666V8a1 1 0 0 1 2 0v1.309l2.783.618a1 1 0 0 0 .434 0L13 9.31V8a1 1 0 1 1 2 0v.864l3-.666V6a1 1 0 0 0-1-1H3Zm15 5.247-3 .666V12a1 1 0 1 1-2 0v-.642l-2.35.522a3.002 3.002 0 0 1-1.3 0L7 11.358V12a1 1 0 1 1-2 0v-1.087l-3-.666V14a1 1 0 0 0 1 1h14a1 1 0 0 0 1-1v-3.753Z"
      />
    </Svg>
  ),
  viewBox: '0 0 20 17',
};

export default SvgPortfolio;

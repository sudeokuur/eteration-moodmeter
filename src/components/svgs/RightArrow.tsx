import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

const SvgRightArrow = {
  svg: (
    <Svg width="24" height="24" fill="none">
      <Path
        fill-rule="evenodd"
        clip-rule="evenodd"
        d="M0.792893 0.292893C1.18342 -0.0976311 1.81658 -0.0976311 2.20711 0.292893L6.20711 4.29289C6.59763 4.68342 6.59763 5.31658 6.20711 5.70711L2.20711 9.70711C1.81658 10.0976 1.18342 10.0976 0.792893 9.70711C0.402369 9.31658 0.402369 8.68342 0.792893 8.29289L4.08579 5L0.792893 1.70711C0.402369 1.31658 0.402369 0.683417 0.792893 0.292893Z"
      />
    </Svg>
  ),
  viewBox: '0 0 7 10',
};

export default SvgRightArrow;

import * as React from 'react';
import Svg, { SvgProps, Path } from 'react-native-svg';
const SvgSearch = {
  svg: (
    <Svg width="24" height="24"  fill="none" >
    <Path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M5.5 0a5.5 5.5 0 1 0 3.118 10.032l2.675 2.675a1 1 0 0 0 1.414-1.414l-2.675-2.675A5.5 5.5 0 0 0 5.5 0ZM2 5.5a3.5 3.5 0 1 1 7 0 3.5 3.5 0 0 1-7 0Z"
    />
  </Svg>
  ),
  viewBox:"0 0 13 13"
}
export default SvgSearch;

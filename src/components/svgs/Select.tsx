import * as React from 'react';
import Svg, { SvgProps, Path } from 'react-native-svg';
const SvgSelect = (props: SvgProps) => (
  <Svg width="24" height="24" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
    <Path d="M6 8a1 1 0 1 0 0 2h6a1 1 0 1 0 0-2H6Z" fill={props.fill} fillOpacity={0.2} />
    <Path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M9 0a9 9 0 1 0 0 18A9 9 0 0 0 9 0ZM2 9a7 7 0 1 1 14 0A7 7 0 0 1 2 9Z"
      fill={props.fill}
      fillOpacity={0.2}
    />
  </Svg>
);
export default SvgSelect;

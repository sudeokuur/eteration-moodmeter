import * as React from 'react';
import Svg, { SvgProps, Rect } from 'react-native-svg';
const SvgSquare = {
  svg: (
    <Svg width="24" height="24" fill="none">
      <Rect width={8} height={8} rx={1} fill="#fff" fillOpacity={0.64} />
    </Svg>
  ), viewBox: "0 0 8 8"
}
export default SvgSquare;

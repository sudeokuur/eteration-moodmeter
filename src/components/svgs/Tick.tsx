import * as React from 'react';
import Svg, { Path, Defs, LinearGradient, Stop } from 'react-native-svg';

const SvgTick = {
  svg: (
    <Svg fill="none">
      <Path
        d="m2 33.818 35.285 35.346a4 4 0 0 0 5.662 0L110 2"
        stroke="url(#tick_svg__a)"
        strokeWidth={4}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <Defs>
        <LinearGradient id="tick_svg__a" x1={110.35} y1={2} x2={-19.271} y2={101.986} gradientUnits="userSpaceOnUse">
          <Stop stopColor="#72FEFF" />
          <Stop offset={1} stopColor="#fff" />
        </LinearGradient>
      </Defs>
    </Svg>
  ),
  viewBox: '0 0 112 73',
};

export default SvgTick;

import * as React from 'react';
import Svg, { Path, Defs, LinearGradient, Stop } from 'react-native-svg';

const SvgUnsuccess = {
  svg: (
    <Svg fill="none">
      <Path
        d="M44.162 71.788a2 2 0 0 0 2.626 1.05c6.497-2.784 11.927-2.784 18.424 0a2 2 0 1 0 1.576-3.676c-7.503-3.216-14.073-3.216-21.576 0a2 2 0 0 0-1.05 2.626Z"
        fill="url(#unsuccess_svg__a)"
        fillOpacity={0.7}
      />
      <Path d="M35 48a2 2 0 1 1 4 0v3a2 2 0 1 1-4 0v-3Z" fill="url(#unsuccess_svg__b)" fillOpacity={0.7} />
      <Path
        d="M75 46a2 2 0 0 0-2 2v3a2 2 0 1 0 4 0v-3a2 2 0 0 0-2-2Z"
        fill="url(#unsuccess_svg__c)"
        fillOpacity={0.7}
      />
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M112 56c0 30.928-25.072 56-56 56S0 86.928 0 56 25.072 0 56 0s56 25.072 56 56Zm-4 0c0 28.719-23.281 52-52 52S4 84.719 4 56 27.281 4 56 4s52 23.281 52 52Z"
        fill="url(#unsuccess_svg__d)"
        fillOpacity={0.7}
      />
      <Defs>
        <LinearGradient
          id="unsuccess_svg__a"
          x1={0}
          y1={-7.498}
          x2={114.049}
          y2={116.993}
          gradientUnits="userSpaceOnUse">
          <Stop stopColor="#0A6DC6" />
          <Stop offset={1} stopColor="#8263D9" />
        </LinearGradient>
        <LinearGradient
          id="unsuccess_svg__b"
          x1={0}
          y1={-7.498}
          x2={114.049}
          y2={116.993}
          gradientUnits="userSpaceOnUse">
          <Stop stopColor="#0A6DC6" />
          <Stop offset={1} stopColor="#8263D9" />
        </LinearGradient>
        <LinearGradient
          id="unsuccess_svg__c"
          x1={0}
          y1={-7.498}
          x2={114.049}
          y2={116.993}
          gradientUnits="userSpaceOnUse">
          <Stop stopColor="#0A6DC6" />
          <Stop offset={1} stopColor="#8263D9" />
        </LinearGradient>
        <LinearGradient
          id="unsuccess_svg__d"
          x1={0}
          y1={-7.498}
          x2={114.049}
          y2={116.993}
          gradientUnits="userSpaceOnUse">
          <Stop stopColor="#0A6DC6" />
          <Stop offset={1} stopColor="#8263D9" />
        </LinearGradient>
      </Defs>
    </Svg>
  ),
  viewBox: '0 0 112 112',
};
export default SvgUnsuccess;

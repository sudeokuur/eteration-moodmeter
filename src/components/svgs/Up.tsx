import * as React from 'react';
import Svg, { Path } from 'react-native-svg';
import { Colors } from 'react-native-ui-lib';

const SvgUp = {
  svg: (
    <Svg width="24" height="24" fill="none">
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M.954 7h6.093C7.573 7 8 6.56 8 6.016c0-.184-.05-.365-.145-.522L4.81.463a.936.936 0 0 0-1.617 0L.145 5.494A1.003 1.003 0 0 0 .448 6.85C.6 6.948.775 7 .954 7Z"
        fill={Colors.greenPrice}
      />
    </Svg>
  ),
  viewBox: '0 0 8 7',
};
export default SvgUp;

import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

const SvgWarning = {
  svg: (
    <Svg width="24" height="24" fill="none">
      <Path d="M7 3a1 1 0 0 1 1 1v3a1 1 0 0 1-2 0V4a1 1 0 0 1 1-1ZM7 11a1 1 0 1 0 0-2 1 1 0 0 0 0 2Z" />
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M7 14A7 7 0 1 0 7 0a7 7 0 0 0 0 14Zm0-2A5 5 0 1 0 7 2a5 5 0 0 0 0 10Z"
      />
    </Svg>
  ),
  viewBox: '0 0 14 14',
};

export default SvgWarning;

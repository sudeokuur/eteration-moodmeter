import pencil from './Pencil';
import warning from './Warning';
import info from './Info';
import cross from './Cross';
import down from './Down';
import up from './Up';
import eye from './Eye';
import arrow from './Arrow';
import back from './Back';
import magnifier from './Magnifier';
import document from './Document';
import edit from './Edit';
import infoWhiteBg from './InfoWhiteBg';
import markets from './Markets';
import portfolio from './Portfolio';
import orders from './Orders';
import ideas from './Ideas';
import home from './Home';
import arrowRight from './RightArrow';
import tick from './Tick';
import ring from './Ring';
import unsuccess from './Unsuccess';
import minus from './Minus';
import plus from './Plus';
import square from './Square';
import late from './Late';
import live from './Live';
import search from './Search';
import downArrow from './DownArrow';
import greySquare from './GreySquare';
import apple from './Apple';

export default {
  pencil,
  warning,
  info,
  cross,
  down,
  up,
  eye,
  arrow,
  back,
  magnifier,
  document,
  edit,
  infoWhiteBg,
  markets,
  portfolio,
  orders,
  ideas,
  home,
  minus,
  plus,
  arrowRight,
  tick,
  ring,
  unsuccess,
  square,
  late,
  live,
  search,
  downArrow,
  greySquare,
  apple,
};

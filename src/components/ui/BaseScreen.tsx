import React from "react";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import { View } from "react-native-ui-lib";
import { Header } from "./Header";
import { useColors } from "../../hooks/useColors";

export function BaseScreen({ children, header }: any) {
  const insets = useSafeAreaInsets();

  const { colors } = useColors();
  return (
    <>
      <View
        style={{
          paddingTop: Math.max(insets.top, 16),

          backgroundColor: colors.secondaryBgStart,
        }}
      >
        {header && <Header header={header} />}
      </View>
      {children}
    </>
  );
}

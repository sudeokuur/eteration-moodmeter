import React, { useState } from 'react';
import { StyleSheet } from 'react-native';
import { Text, TouchableOpacity } from 'react-native-ui-lib';
import { fonts } from '../../../themes';
import { useColors } from '../../../hooks/useColors';

export function ButtonDanger(props: any) {
  const { disabled, label, onPress, description, size, style } = props;
  const [pressed, setPressed] = useState(false);
  const { colors } = useColors();
  let btnSize;

  switch (size) {
    case 'xsmall':
      btnSize = 4;
      break;
    case 'small':
      btnSize = 8;
      break;
    case 'medium':
      btnSize = 12;
      break;
    default:
      btnSize = 16;
      break;
  }

  return (
    <TouchableOpacity
      onPress={onPress}
      activeOpacity={1}
      disabled={disabled}
      onPressIn={() => setPressed(true)}
      onPressOut={() => setPressed(false)}
      pressed={pressed}
      activeBackgroundColor={colors.$btnDangerPressed}
      style={[styles(props, btnSize, colors).touchableOpacity, style]}>
      <Text style={styles(props, size, colors).title}>{label}</Text>
      {description && <Text button2 color={colors.$btnSuccessDescriptionColor}>{description}</Text>}
    </TouchableOpacity>
  );
}

const styles = (props: any, size: any, colors: any) =>
  StyleSheet.create({
    touchableOpacity: {
      backgroundColor: props.disabled ? colors.$btnDangerDisabled : colors.$btnDanger,
      paddingTop: props.description ? 8 : size,
      paddingBottom: props.description ? 10 : size,
      borderRadius: 8,
      justifyContent: 'center',
      alignItems: 'center',
    },
    title: {
      fontFamily: fonts.medium,
      fontSize: props.description ? 14 : 12,
      lineHeight: props.description ? 24 : 16,
      fontWeight: '500',
      color: colors.$btnSuccessLabelColor,
    },
  });

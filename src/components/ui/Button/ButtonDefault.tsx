import React, { useState } from 'react';
import { Button } from 'react-native-ui-lib';

export function ButtonDefault(props: any) {
  const [pressed, setPressed] = useState(false);

  return (
    <Button
      {...props}
      activeOpacity={1}
      onPressIn={() => setPressed(true)}
      onPressOut={() => setPressed(false)}
      pressed={pressed} />
  );
}

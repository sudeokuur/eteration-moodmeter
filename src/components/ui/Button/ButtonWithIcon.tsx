/* eslint-disable no-nested-ternary */
import React, { useState } from 'react';
import { Button } from 'react-native-ui-lib';
import { Icon } from '../Icon';
import { useColors } from '../../../hooks/useColors';

export function ButtonWithIcon(props: any) {
  const { iconName, outline, disabled, secondary } = props;
  const [pressed, setPressed] = useState(false);
  const { colors } = useColors();

  const fillColor = () => {
    let fillColorValue = pressed ? colors.actionDark : colors.$btnPrimaryText;

    if (outline) {
      fillColorValue = colors.actionDark;

      if (secondary) {
        fillColorValue = pressed ? colors.$btnOutlineSecondaryPressedIcon : colors.$btnOutlineSecondaryIcon;
      }
      if (disabled) {
        fillColorValue = colors.$btnOutlineSecondaryDisabledIcon;
      }
    } else if (secondary) {
      fillColorValue = colors.$btnSecondaryIcon;
      if (disabled) {
        fillColorValue = colors.$btnSecondaryDisabledText;
      }
    }

    return fillColorValue;
  };

  return (
    <Button
      {...props}
      activeOpacity={1}
      onPressIn={() => setPressed(true)}
      onPressOut={() => setPressed(false)}
      pressed={pressed}
      iconSource={() => <Icon name={iconName} width={12} height={12} fill={fillColor()} style={{ marginRight: 10 }} />}
    />
  );
}

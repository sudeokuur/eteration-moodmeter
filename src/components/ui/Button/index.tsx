import React from 'react';
import { ButtonDefault } from './ButtonDefault';
import { ButtonWithIcon } from './ButtonWithIcon';
import { ButtonSuccess } from './ButtonSuccess';
import { ButtonDanger } from './ButtonDanger';

export function Button(props: any) {
  const { iconName, success, danger } = props;

  const getComponent = () => {
    let component = <ButtonDefault {...props} />;
    if (iconName) {
      component = <ButtonWithIcon {...props} />;
    } else if (success) {
      component = <ButtonSuccess {...props} />;
    } else if (danger) {
      component = <ButtonDanger {...props} />;
    }
    return component;
  };
  return getComponent();
}

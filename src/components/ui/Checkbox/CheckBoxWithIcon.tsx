import React, { useEffect } from 'react';
import { Checkbox, Colors, TouchableOpacity, View } from 'react-native-ui-lib';
import { Icon } from '../Icon';
import { SIZE, BORDER_RADIUS, ICON_SIZE, LABEL_SIZE, MARGIN_HORIZONTAL } from './defaultValues';
type Props = {
  value: boolean;
  onValueChange: () => void;
  disable: boolean;
  label: string;
  labelStyle: object;
  boxStyle: object;
  selectedIcon: any;
  size: number;
  borderRadius: number;
  checkIconColor: string;
  color: string;
  key: any;
  description: string;
  infoIcon: React.ReactNode;
  secondIcon: React.ReactNode;
  outline: boolean;
  icon: string;
  iconSize: number;
  labelSize: number;
  onpress: () => void;
  marginHorizontalLabel: number;
  iconColor: string;
  labelType: string
};

export function GYCheckBoxWithIcon({
  value,
  onValueChange,
  disable = false,
  label = "label",
  labelStyle = {},
  boxStyle = {},
  selectedIcon,
  key,
  size = SIZE,
  borderRadius = BORDER_RADIUS,
  labelSize = LABEL_SIZE,
  checkIconColor = Colors.$checkboxIconColor,
  color = Colors.$checkboxBg,
  outline,
  icon,
  iconSize = ICON_SIZE,
  iconColor = Colors.$checkboxSecondaryIconColor,
  marginHorizontalLabel = MARGIN_HORIZONTAL,
  labelType = "1",
  onpress
}: Props) {
  let lt = {}
  if (disable) {
    boxStyle = {
      ...boxStyle,
      color: Colors.$iconDisabled,
      borderColor: Colors.$iconDisabled,
    };
    labelStyle = {
      ...labelStyle,
      color: Colors.$checkboxPrimaryTextInActive,
      fontSize: labelSize
    };
  } else {
    labelStyle = {
      ...labelStyle,
      color: Colors.$checkboxPrimaryTextActive,
      fontSize: labelSize
    },
      boxStyle = {
        ...boxStyle,
        borderColor: Colors.$checkboxBorderColor
      }
    lt = {
      fontWeight: labelType === "1" ? '500' : '300',
      color: labelType === "1" ? Colors.$checkboxPrimaryTextActive
        :
        (labelType === "2" ? Colors.$checkboxPrimaryTextActive : Colors.$checkboxThirdText)
    };
  }



  return (
    <View flex row centerV>
      <Checkbox
        value={value}
        onValueChange={onValueChange}
        borderRadius={borderRadius}
        color={color || Colors.action}
        iconColor={checkIconColor}
        disabled={disable}
        size={size}
        style={boxStyle}
        key={key}
        outline={outline}
        selectedIcon={selectedIcon & selectedIcon}
        label={label}
        labelStyle={{
          ...labelStyle,
          fontSize: labelSize,
          marginHorizontal: marginHorizontalLabel,

          ...lt
        }}
      />
      {icon && <TouchableOpacity onPress={() => onpress()} center marginT-2>
        <Icon name={icon} fill={iconColor} width={iconSize} height={iconSize} />
      </TouchableOpacity>}
    </View>
  );
}
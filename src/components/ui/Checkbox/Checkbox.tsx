import React from 'react';
import { StyleSheet } from 'react-native';
import { Checkbox, Colors } from 'react-native-ui-lib';
type Props = {
  value: boolean;
  onValueChange: () => void;
  disable: boolean;
  label: string;
  labelStyle: object;
  boxStyle: object;
  selectedIcon: any;
  size: number;
  borderRadius: number;
  iconColor: string;
  color: string;
  key: any;
  description: string;
  infoIcon: React.ReactNode;
  outline: boolean;
};

export function GYCheckBox({
  value,
  onValueChange,
  disable = false,
  boxStyle = {},
  selectedIcon,
  key,
  size = 24,
  borderRadius = 4,
  iconColor = Colors.$checkboxIconColor,
  color = Colors.$checkboxBg,
  outline,
}: Props) {
  if (disable) {
    boxStyle = {
      ...boxStyle,
      color: Colors.$iconDisabled,
      borderColor: Colors.$iconDisabled,
    };
  } else {
    boxStyle = {
      ...boxStyle,
      borderColor: Colors.$checkboxBorderColor,
    };
  }
  return (
    <Checkbox
      value={value}
      onValueChange={onValueChange}
      borderRadius={borderRadius}
      color={color}
      iconColor={iconColor}
      disabled={disable}
      size={size}
      style={{ ...styles.cb, ...boxStyle }}
      key={key}
      outline={outline}
      selectedIcon={selectedIcon & selectedIcon}
    />
  );
}

const styles = StyleSheet.create({
  cb: {
    borderColor: Colors.$checkboxBorderColor,
  },
});

import React from 'react';
import { StyleSheet } from 'react-native';
import { Checkbox, Colors, Text, View } from 'react-native-ui-lib';
import { SIZE, BORDER_RADIUS } from './defaultValues';
import { Icon } from '../Icon';

type Props = {
  value: boolean;
  onValueChange: () => void;
  disable: boolean;
  label: string;
  labelStyle: object;
  boxStyle: object;
  selectedIcon: any;
  size: number;
  borderRadius: number;
  iconColor: string;
  color: string;
  key: any;
  description: string;
  infoIcon: React.ReactNode;
  secondIcon: React.ReactNode;
  outline: boolean;
};

export function GYCheckBoxWithDes({
  value,
  onValueChange,
  disable = false,
  label = "",
  description = "",
  boxStyle = {},
  selectedIcon,
  key,
  size = SIZE,
  borderRadius = BORDER_RADIUS,
  iconColor = Colors.$checkboxIconColor,
  color = Colors.$checkboxBg,
  outline,
  secondIcon,
}: Props) {
  if (disable) {
    boxStyle = {
      ...boxStyle,
      color: Colors.$iconDisabled,
      borderColor: Colors.$iconDisabled,
    };
  } else {
      boxStyle = {
        ...boxStyle,
        borderColor: Colors.$checkboxBorderColor
      }
  }
  return (
    <View flex row centerV style={disable ? styles.noDisable : styles.disabale}>
      <Checkbox
        value={value}
        onValueChange={onValueChange}
        borderRadius={borderRadius}
        color={color}
        iconColor={iconColor}
        disabled={disable}
        size={size}
        style={boxStyle}
        outline={outline}
        key={key}
        selectedIcon={selectedIcon & selectedIcon}
      />
      <View width={size*1.3} height={size*1.3} marginH-8 center>
        {secondIcon && secondIcon}
      </View>
      <View >
        <Text heading4 color={disable ? Colors.$checkboxPrimaryTextInActive : Colors.$checkboxPrimaryTextActive}>{label}</Text>
        <Text heading7 color={disable ? Colors.$checkboxSecondaryTextInActtextDisabledive : Colors.$checkboxSecondaryTextActive}>{description}</Text>
      </View>
    </View>
  );
}
const staticStyle = {
  width: "100%",
  padding: 11,
  borderRadius: 8,
}
const styles = StyleSheet.create({
  disabale: {
    backgroundColor: Colors.$checkboxContainerBg,
    ...staticStyle
  },
  noDisable: {
    backgroundColor: Colors.$checkboxContainerBgDisabled,
    ...staticStyle,
  }
})
import React from 'react';
import { StyleSheet } from 'react-native';
import { Checkbox, Colors } from 'react-native-ui-lib';
import { SIZE, BORDER_RADIUS, LABEL_SIZE } from './defaultValues';
type Props = {
  value: boolean;
  onValueChange: () => void;
  disable: boolean;
  label: string;
  labelStyle: object;
  boxStyle: object;
  selectedIcon: any;
  size: number;
  borderRadius: number;
  iconColor: string;
  color: string;
  key: any;
  description: string;
  infoIcon: React.ReactNode;
  outline: boolean;
  labelSize: number;
};



export function GYCheckBoxWithLabel({
  value,
  onValueChange,
  disable = false,
  boxStyle = {},
  selectedIcon,
  label,
  key,
  labelStyle,
  size = SIZE,
  labelSize = LABEL_SIZE,
  borderRadius = BORDER_RADIUS,
  iconColor = Colors.$checkboxIconColor,
  color = Colors.$checkboxBg,
  outline
}: Props) {
  if (disable) {
    boxStyle = {
      ...boxStyle,
      color: Colors.$iconDisabled,
      borderColor: Colors.$iconDisabled,
    };
    labelStyle = {
      ...labelStyle,
      color: Colors.$checkboxPrimaryTextInActive,
      fontSize: labelSize
    };
  } else {
    labelStyle = {
      ...labelStyle,
      color: Colors.$checkboxPrimaryTextActive,
      fontSize: labelSize
    },
      boxStyle = {
        ...boxStyle,
        borderColor: Colors.$checkboxBorderColor
      }
  }
  return (
    <Checkbox
      value={value}
      onValueChange={onValueChange}
      borderRadius={borderRadius}
      color={color}
      iconColor={iconColor}
      label={label}
      disabled={disable}
      size={size}
      labelStyle={labelStyle}
      style={boxStyle}
      key={key}
      selectedIcon={selectedIcon & selectedIcon}
      outline={outline}
    />
  );
}






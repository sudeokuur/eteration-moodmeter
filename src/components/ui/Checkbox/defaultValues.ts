export const SIZE = 24
export const ICON_SIZE=14
export const LABEL_SIZE=14
export const BORDER_RADIUS=4
export const MARGIN_HORIZONTAL=9
import React from 'react';
import { GYCheckBox } from './Checkbox';
import { GYCheckBoxWithLabel } from './CheckboxWithLabel';
import { GYCheckBoxWithDes } from './CheckboxWithDes';
import { GYCheckBoxWithIcon } from './CheckBoxWithIcon';

export function Checkbox(props: any) {
  const { type } = props;

  const getComponent = () => {
    let component = <GYCheckBox {...props} />;
    if (type==="label") {
      component = <GYCheckBoxWithLabel {...props} />;
    } else if (type==="description") {
      component = <GYCheckBoxWithDes {...props} />
    } else if (type==="icon" ) {
      component = <GYCheckBoxWithIcon {...props} />
    }
    return component;
  };
  return getComponent();
}

import React, { useLayoutEffect, useRef } from "react";
import { StatusBar } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { Text, TouchableOpacity, View } from "react-native-ui-lib";
import { ProfileImage } from "./ProfileImage";
import { useReportBack } from "../../context";
import { Icon } from "./Icon";
import { useColors } from "../../hooks/useColors";

type HeaderProps = {
  header: {
    title: string;
    showSearchIcon?: boolean;
    showProfileImage?: boolean;
    back?: boolean;
    statusBarType?: "light-content" | "dark-content";
    lightTitle?: boolean;
    backAction: Function;
  };
  onChangeHeaderHeight?: Function;
};

export function Header({ header }: HeaderProps) {
  const { title, back, statusBarType, lightTitle, backAction } = header;
  const { theme } = useReportBack().store;
  const navigation = useNavigation();
  const containerRef = useRef<View>(null);
  const { colors } = useColors();

  return (
    <View ref={containerRef} row centerV spread marginV-8 marginH-16>
      <StatusBar
        backgroundColor={colors.secondaryBgStart}
        barStyle={statusBarType || theme === "dark" || lightTitle ? "light-content" : "dark-content"}
      />
      {back && (
        <TouchableOpacity onPress={backAction || navigation.goBack}>
          <Icon name="back" width={20} height={20} fill={colors.primaryText} />
        </TouchableOpacity>
      )}
      <Text heading1 marginR-32={back} color={colors.white}>
        {title}
      </Text>
    </View>
  );
}

import React from "react";
import { StatusBar, StyleSheet } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { Colors, Text, TouchableOpacity, View } from "react-native-ui-lib";
import { useReportBack } from "../../context";
import { Back } from "../svgs";

type Props = {
  title: string;
  backButtonVisible: boolean;
  customizeBackAction?: () => void;
};

export function HeaderMedium(props: Props) {
  const { title, backButtonVisible, customizeBackAction } = props;
  const {
    store: { theme },
  } = useReportBack();

  const navigation = useNavigation();

  const handleBackButtonPress = () => {
    if (customizeBackAction) {
      customizeBackAction();
    } else {
      navigation.goBack();
    }
  };

  return (
    <View row centerV margin-24>
      <StatusBar backgroundColor={Colors.background} barStyle={theme === "dark" ? "light-content" : "dark-content"} />
      {backButtonVisible && (
        <TouchableOpacity style={[styles.backIconContainer]} center onPress={handleBackButtonPress}>
          <Back width={8} height={14} fill={Colors.secondaryText} />
        </TouchableOpacity>
      )}
      <Text heading3 marginL-16 primaryText>
        {title}
      </Text>
    </View>
  );
}
const styles = StyleSheet.create({
  backIconContainer: {
    width: 24,
    height: 24,
  },
});

/* eslint-disable react/destructuring-assignment */
import React from 'react';
import Svg from 'react-native-svg';
import svgSet from '../svgs/svgSet';

export function Icon(props: any) {
  const name = svgSet[props.name as keyof typeof svgSet];
  if (!name) {
    return null;
  }

  const height = props.height ? props.height.toString() : '44';
  const width = props.width ? props.width.toString() : '44';
  const strokeWidth = props.strokeWidth && props.strokeWidth.toString();

  const isSimple = React.isValidElement(name);
  const svgEl = isSimple ? name : name.svg;

  let viewBox;

  if (props.viewBox && props.viewBox !== '0 0 100 100') {
    viewBox = props.viewBox;
  } else if (!isSimple && name.viewBox) {
    viewBox = name.viewBox;
  } else if (props.defaultViewBox) {
    viewBox = props.defaultViewBox;
  } else {
    viewBox = '0 0 100 100';
  }

  return (
    <Svg height={height} width={width} viewBox={viewBox} style={props.style}>
      {React.cloneElement(svgEl, {
        fill: props.fill ? props.fill : '#000000',
        fillRule: props.fillRule ? props.fillRule : 'evenodd',
        stroke: props.stroke,
        strokeWidth,
      })}
    </Svg>
  );
}

import React, { useEffect, useState } from 'react';
import { Platform } from 'react-native';
import { Text, TextField, View } from 'react-native-ui-lib';
import { useTranslation } from 'react-i18next';

import { Icon } from '../Icon';
import { useColors } from '../../../hooks/useColors';

const TOP_SIZE = Platform.select({ ios: 26, android: 32 });

export function AmountInput(props: any) {
  const { validateOnBlur = true, validationMessage, value, currency = 'TL' } = props;

  const [isValid, setIsValid] = useState(true);
  const [onFocus, setOnFocus] = useState(false);
  const [val, setVal] = useState('');
  const { i18n } = useTranslation();
  const enLanguage = i18n.language === 'en';
  const filterRegex = enLanguage ? /[^\d.]/g : /[^\d,]/g;
  const integerPartRegex = /\B(?=(\d{3})+(?!\d))/g;
  const { colors } = useColors();

  useEffect(() => {
    if (value !== undefined) {
      const filteredValue = value.toString().replace(filterRegex, '');
      formattedText(filteredValue);
    }
  }, [value]);

  const onChange = (text: any) => {
    const filteredText = text.replace(filterRegex, '');
    const decimalCount = (filteredText.match(enLanguage ? /\./g : /,/g) || []).length;

    if (decimalCount > 1) return;

    formattedText(filteredText);
  };

  const formattedText = (filteredValue: string) => {
    const decimalSeparate = enLanguage ? '.' : ',';
    const integerSeparate = enLanguage ? ',' : '.';
    const hasDecimalPoint = filteredValue.indexOf(decimalSeparate) !== -1;

    if (hasDecimalPoint) {
      const [integerPart, decimalPart] = filteredValue.split(decimalSeparate);

      // this line to prevent deleting the first dot
      if (decimalPart === '') {
        setVal(`${filteredValue.replace(integerPartRegex, integerSeparate)}`);
        return;
      }

      const limitedDecimalPart = decimalPart ? decimalPart.slice(0, 2) : '';
      const formattedValue = `${integerPart.replace(
        integerPartRegex,
        integerSeparate,
      )}${decimalSeparate}${limitedDecimalPart}`;

      setVal(formattedValue);
    } else {
      const formattedValue = `${filteredValue.replace(integerPartRegex, integerSeparate)}`;

      setVal(formattedValue);
    }
  };

  const customValidationMessageTemplate = () =>
    validationMessage?.map((item: string) => (
      <View row>
        <Icon name="warning" width={24} height={24} fill={colors.$inputValidationColor} />
        <Text body2 color={colors.$inputValidationColor}>
          {item}
        </Text>
      </View>
    ));

  return (
    <View style={{ position: 'relative' }}>
      <TextField
        {...props}
        onChangeValidity={e => setIsValid(e)}
        isValid={isValid}
        validateOnBlur={validateOnBlur}
        validationMessage={customValidationMessageTemplate()}
        onFocus={() => setOnFocus(true)}
        onBlur={() => setOnFocus(false)}
        onChangeText={e => onChange(e)}
        isFocused={onFocus}
        enableErrors
        keyboardType="numeric"
        value={val}
      />

      <View
        padding-16
        style={{
          position: 'absolute',
          top: TOP_SIZE,
          right: 0,
        }}>
        <Text body1 color={colors.$inputCurrencyColor}>
          {currency}
        </Text>
      </View>
    </View>
  );
}

import React, { useState } from 'react';
import { TextField, View, TouchableOpacity, Text } from 'react-native-ui-lib';
import { Platform } from 'react-native';

import { Icon } from '../index';
import { useColors } from '../../../hooks/useColors';

const TOP_SIZE = Platform.select({ ios: 36, android: 42 });

export function CounterInput(props: any) {
  const { validateOnBlur = true, validationMessage, value = '0', increaseRatio = 1, onChangeText } = props;
  const [isValid, setIsValid] = useState(true);
  const [onFocus, setOnFocus] = useState(false);
  const [val, setVal] = useState(parseFloat(value));
  const { colors } = useColors();

  const customValidationMessageTemplate = () =>
    validationMessage?.map((item: string) => (
      <View row>
        <Icon name="warning" width={24} height={24} fill={colors.$inputValidationColor} />
        <Text body2 color={colors.$inputValidationColor}>
          {item}
        </Text>
      </View>
    ));

  const increaseValue = () => {
    setVal(val + increaseRatio);
    onChangeText(val);
  };

  const decreaseValue = () => {
    setVal(val - increaseRatio);
    onChangeText(val);
  };

  function onChange(e: any) {
    if (Number.isNaN(Number(e)) || !Number.isFinite(Number(e))) {
      onChangeText(val);
    } else if (e.length === 0) {
      setVal(0);
      onChangeText(0);
    } else {
      setVal(parseFloat(e));
      onChangeText(e);
    }
  }

  return (
    <View style={{ position: 'relative' }}>
      <TextField
        {...props}
        onChangeValidity={e => setIsValid(e)}
        isValid={isValid}
        validateOnBlur={validateOnBlur}
        validationMessage={customValidationMessageTemplate()}
        onFocus={() => setOnFocus(true)}
        onBlur={() => setOnFocus(false)}
        onChangeText={e => onChange(e)}
        isFocused={onFocus}
        enableErrors
        textAlign="center"
        keyboardType="numeric"
        value={val.toString()}
      />

      <TouchableOpacity
        onPress={() => decreaseValue()}
        style={{
          position: 'absolute',
          top: TOP_SIZE,
          left: 0,
          width: 32,
          height: 32,
          padding: 10,
          borderRadius: 24,
          backgroundColor: colors.$inputIncreaseBackgroundColor,
          marginHorizontal: 16,
        }}>
        <Icon name="minus" fill={colors.$inputPlusMinusIconColor} width={12} height={12} />
      </TouchableOpacity>

      <TouchableOpacity
        onPress={() => increaseValue()}
        backgroundColor={colors.$inputIncreaseBackgroundColor}
        style={{
          position: 'absolute',
          top: TOP_SIZE,
          right: 0,
          width: 32,
          height: 32,
          padding: 10,
          borderRadius: 24,
          marginHorizontal: 16,
        }}>
        <Icon name="plus" fill={colors.$inputPlusMinusIconColor} width={12} height={12} />
      </TouchableOpacity>
    </View>
  );
}

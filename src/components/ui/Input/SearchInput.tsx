import React, { useRef, useState } from 'react';
import { TextField, View, TouchableOpacity, Text } from 'react-native-ui-lib';
import { useTranslation } from 'react-i18next';
import { StyleSheet, Platform } from 'react-native';
import { Icon } from '../index';
import { useColors } from '../../../hooks/useColors';

export function SearchInput(props: any) {
  const [onFocus, setOnFocus] = useState(false);
  const textInputReference = useRef(null);
  const [isValid, setIsValid] = useState(true);
  const { colors } = useColors();
  const { t } = useTranslation();

  return (
    <View style={styles.container}>
      <View absV style={styles.searchStyle}>
        <Icon name="search" fill={colors.secondaryText} width={13} height={13} />
      </View>
      <TextField
        {...props}
        ref={textInputReference}
        onFocus={() => setOnFocus(true)}
        onBlur={() => setOnFocus(false)}
        isFocused={onFocus}
        onChangeValidity={e => setIsValid(e)}
        isValid={isValid}
      />

      {onFocus === true && (
        <TouchableOpacity onPress={() => textInputReference?.current?.clear()} style={styles.clearStyle}>
          <Text heading7 color={colors.secondaryText}>
            {t('clear')}
          </Text>
        </TouchableOpacity>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    marginVertical: 16,
  },
  clearStyle: {
    position: 'absolute',
    top: Platform.OS == "ios" ? -15 : -8,
    right: 0,
    padding: 16,
  },
  searchStyle: {
    top: Platform.OS == "ios" ? 1 : 8,
    left: 0,
  },
});

import React, { useRef, useState } from 'react';
import { Platform } from 'react-native';
import { TextField as UITextField, View, TouchableOpacity, Text, Hint } from 'react-native-ui-lib';
import { Icon } from '../index';
import { useColors } from '../../../hooks/useColors';

const TOP_SIZE = Platform.select({ ios: 32, android: 38 });

export function TextField(props: any) {
  const { validateOnBlur = true, label, info, validationMessage, showCharCounter } = props;

  const [isValid, setIsValid] = useState(true);
  const [onShowHint, setOnShowHint] = useState(false);
  const [onFocus, setOnFocus] = useState(false);
  const textInputReference = useRef(null);
  const { colors } = useColors();

  const customLabel = () => {
    if (info) {
      return (
        <View centerV row>
          <Text marginR-8 body2 color={colors.$inputLabelColor}>
            {label}
          </Text>
          <TouchableOpacity onPress={() => setOnShowHint(true)}>
            <Hint visible={onShowHint} message={info} onBackgroundPress={() => setOnShowHint(false)}>
              <View>
                <Icon name="info" fill={colors.$inputLabelColor} width={14} height={14} />
              </View>
            </Hint>
          </TouchableOpacity>
        </View>
      );
    }

    return label;
  };

  const customValidationMessageTemplate = () =>
    validationMessage?.map((item: string) => (
      <View row centerV paddingV-8>
        <Icon name="warning" width={14} height={14} fill={colors.$inputValidationColor} />
        <Text body2 color={colors.$inputValidationColor} marginL-8>
          {item}
        </Text>
      </View>
    ));

  return (
    <View style={{ position: 'relative' }}>
      <UITextField
        {...props}
        label={customLabel()}
        onChangeValidity={e => setIsValid(e)}
        isValid={isValid}
        validateOnBlur={validateOnBlur}
        validationMessage={customValidationMessageTemplate()}
        ref={textInputReference}
        onFocus={() => setOnFocus(true)}
        onBlur={() => setOnFocus(false)}
        isFocused={onFocus}
        showCharCounter={!(!showCharCounter || !onFocus)}
        enableErrors
      />

      {onFocus === true && (
        <TouchableOpacity
          onPress={() => textInputReference?.current?.clear()}
          style={{ position: 'absolute', top: TOP_SIZE, right: 0, padding: 16 }}>
          <Icon name="cross" fill={colors.$inputCounterColor} width={8} height={8} />
        </TouchableOpacity>
      )}
    </View>
  );
}

import React from "react";
import { StyleSheet } from "react-native";
import { Image, TouchableOpacity, View } from "react-native-ui-lib";
import { useNavigation } from "@react-navigation/native";
import { Icon } from "./Icon";
import { useReportBack } from "../../context";

export function ProfileImage() {
  const navigation = useNavigation();
  const {
    store: { user },
  } = useReportBack();

  return (
    <TouchableOpacity margin-20 onPress={() => navigation.navigate("profile")}>
      <View centerH centerV>
        <Image style={styles.tinyLogo} source={{ uri: user?.photo }} />
        <Icon name="frame" width={60} height={60} />
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: { flex: 1, justifyContent: "center", alignItems: "center" },
  tinyLogo: {
    position: "absolute",
    width: 44,
    height: 44,
    borderRadius: 22,
    zIndex: -100,
  },
});

import { StyleSheet, Animated, TouchableNativeFeedback, ViewStyle } from 'react-native';
import { View, Colors } from 'react-native-ui-lib';
import React, { useState, useEffect } from 'react';

type Props = {
  selected: boolean;
  onChangeSelected: (newValue: boolean) => void;
  style?: ViewStyle;
};

RadioButton.defaultProps = {
  style: {},
};

export function RadioButton(props: Props) {
  const { selected, onChangeSelected, style } = props;
  const [scaleValue] = useState(new Animated.Value(selected ? 1 : 0));

  useEffect(() => {
    Animated.timing(scaleValue, {
      toValue: selected ? 1 : 0,
      duration: 250,
      useNativeDriver: true,
    }).start();
  }, [selected, scaleValue]);

  const handlePress = () => {
    if (onChangeSelected) {
      onChangeSelected(!selected);
    }
  };

  return (
    <TouchableNativeFeedback onPress={handlePress}>
      <View
        style={[
          styles.container,
          selected ? styles.active : styles.passive,
          style,
        ]}>
        {selected && (
          <Animated.View
            style={[
              styles.ellipse,
              {
                transform: [{ scale: scaleValue }],
              },
            ]} />
        )}
      </View>
    </TouchableNativeFeedback>
  );
}

const styles = StyleSheet.create({
  container: {
    width: 24,
    height: 24,
    borderRadius: 12,
    justifyContent: 'center',
    alignItems: 'center',
  },
  active: {
    backgroundColor: Colors.action,
  },
  passive: {
    backgroundColor: 'rgba(255, 255, 255, 0.08)',
    borderColor: 'rgba(255, 255, 255, 0.2)',
    borderWidth: StyleSheet.hairlineWidth,
  },
  ellipse: {
    width: 10,
    height: 10,
    borderRadius: 5,
    backgroundColor: Colors.white,
  },
});

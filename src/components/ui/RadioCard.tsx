import React from 'react';
import { StyleSheet, TouchableNativeFeedback } from 'react-native';
import { View, Text, Typography } from 'react-native-ui-lib';
import { RadioButton } from './RadioButton';

type Props = {
  label: string;
  selected: boolean;
  onChangeSelected: (newValue: boolean) => void;
};

export function RadioCard(props: Props) {
  const { selected, onChangeSelected, label } = props;

  const handlePress = () => {
    onChangeSelected?.(!selected);
  };

  return (
    <TouchableNativeFeedback onPress={handlePress}>
      <View style={[styles.container, { backgroundColor: selected ? '#2DCCCD1A' : 'transparent' }]}>
        <RadioButton onChangeSelected={handlePress} selected={selected} />
        <Text
          style={selected ? Typography.heading6 : Typography.body1}
          heading6={selected}
          primaryText
          numberOfLines={1}
          marginL-8
          flex>
          {label}
        </Text>
      </View>
    </TouchableNativeFeedback>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flexDirection: 'row',
    marginHorizontal: 19,
    borderRadius: 8,
    marginBottom: 4,
    minHeight: 56,
    padding: 16,
  },
});

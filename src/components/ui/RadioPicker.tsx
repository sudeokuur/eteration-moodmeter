import React, { useState, useEffect } from 'react';
import { VirtualizedList } from 'react-native';
import { View, Dialog } from 'react-native-ui-lib';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { t } from 'i18next';

import { RadioCard } from './RadioCard';
import { Header } from '../ui';

type RadioObject = {
  description: string;
  selected: boolean;
};

type Props = {
  dataKey: string;
  isOpen: boolean;
  data: RadioObject[];
  atLeastOneChoice?: boolean;
  allowMultipleSelection?: boolean;
  onDismiss: () => void;
  onValueChange: (newData: RadioObject[], dataKey: string) => void;
};

RadioPicker.defaultProps = {
  atLeastOneChoice: false,
  allowMultipleSelection: false,
};

export function RadioPicker(props: Props) {
  const {
    dataKey,
    isOpen,
    data,
    onDismiss,
    onValueChange,
    atLeastOneChoice = false,
    allowMultipleSelection = false,
  } = props;

  const [internalData, setInternalData] = useState<RadioObject[]>([]);
  const insets = useSafeAreaInsets();

  useEffect(() => {
    setInternalData([...data]);
  }, [data]);

  const handleDismiss = () => onDismiss?.();

  const handleRadioSelect = (selectedIndex: number, newValue: boolean) => {
    const newData = internalData.map((item, i) =>
      allowMultipleSelection || i === selectedIndex
        ? { ...item, selected: i === selectedIndex ? newValue : item.selected }
        : { ...item, selected: false },
    );

    if (atLeastOneChoice && !newData.some(item => item.selected)) {
      newData[selectedIndex].selected = true;
    }

    setInternalData(newData);
    onValueChange?.(newData, dataKey);
  };

  return (
    <Dialog panDirection="right" width="100%" height="100%" visible={isOpen} onDismiss={handleDismiss}>
      <View
        style={{
          paddingTop: Math.max(insets.top, 0),
        }}
        bg-background
        flex>
        <Header
          header={{
            title: t('appTheme'),
            back: true,
            backAction: handleDismiss,
            lightTitle:false,
          }}
        />
        <VirtualizedList
          getItem={(_data, i) => _data[i]}
          getItemCount={__data => __data.length}
          keyExtractor={(_, i) => i.toString()}
          data={internalData}
          renderItem={({ item, index }) => {
            const { description, selected } = item as RadioObject;
            return (
              <RadioCard
                label={description}
                selected={selected}
                onChangeSelected={newValue => handleRadioSelect(index, newValue)}
              />
            );
          }}
        />
      </View>
    </Dialog>
  );
}

import React from 'react';
import { StyleSheet, TouchableNativeFeedback } from 'react-native';
import { View, Text } from 'react-native-ui-lib';
import { Icon } from '../ui/Icon';
import { useColors } from '../../hooks/useColors';

type Props = {
  label: string;
  description?: string;
  iconName?: string;
  onPress: () => void;
};

SettingSelectionCard.defaultProps = {
  description: '',
  iconName: '',
};

export function SettingSelectionCard(props: Props) {
  const { label, description, iconName, onPress } = props;
  const { colors } = useColors();

  return (
    <TouchableNativeFeedback onPress={onPress}>
      <View style={styles.cardContainer}>
        <View style={styles.card}>
          {iconName && <Icon name={iconName} width={18} height={18} fill={colors.secondaryText} style={styles.icon} />}
          <View style={styles.content}>
            <Text heading6 primaryText marginR-24 numberOfLines={2} style={styles.label}>
              {label}
            </Text>
            <Text body1 primaryText flex marginR-13 numberOfLines={1} style={styles.description}>
              {description}
            </Text>
          </View>
          <Icon name="arrowRight" width={6} height={10} color={colors.secondaryText} />
        </View>
        <View style={styles.separator} />
      </View>
    </TouchableNativeFeedback>
  );
}
const styles = StyleSheet.create({
  cardContainer: {
    marginHorizontal: 24,
  },
  card: {
    flexDirection: 'row',
    alignItems: 'center',
    minHeight: 48,
  },
  content: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  label: {
    maxWidth: '50%',
  },
  description: {
    textAlign: 'right',
  },
  icon: {
    marginRight: 19,
  },
  separator: {
    height: 1,
    backgroundColor: 'rgba(255, 255, 255, 0.2)',
    opacity: 0.2,
  },
});

import axios from "axios";

import { createContext, useContext } from "react";
import { SchemeType } from "react-native-ui-lib";
import Config from "react-native-config";

import { ReportBackStore } from "./ReportBackStore";
import tokenInjector from "./axios";

if (process?.env?.NODE_ENV === "development") {
  console.log(`DID YOU UPDATE YOUR CURRENT API URL?: ${Config.API_URL}`);
}

export const apiClient = axios.create({
  baseURL: Config.API_URL,
  timeout: 10000,
  withCredentials: false,
  headers: { "Content-Type": "application/json" },
});

tokenInjector(apiClient, {
  shouldIntercept: (error: any) =>
    error?.response?.status === 403 ||
    error?.response?.status === 401 ||
    error?.response?.data?.errorCode === "EXPIRED_ACCESS_TOKEN",
});

export type ReportBackStoreType = {
  user?: any;
  stack?: any;
  error?: any;
  loading?: any;
  session?: any;
  message?: any;
  overviewList?: any;
  theme?: SchemeType;
  login: Function;
  logout: Function;
  isUserSessionStarted: Function;
  setUser: Function;
  getUser: Function;
  setMessage: Function;
  setStack: Function;
  setError: Function;
  setLoading: Function;
  setSession: Function;
  reset: Function;
  setTheme: Function;
  setOverviewList: Function;
};

export type ContextState = {
  store: ReportBackStoreType;
  apiClient: any;
  dropdownAlert: any;
  message: any;
  theme: SchemeType;
};

export const rnBoilerplateStore = new ReportBackStore();

export const ReportBackContext = createContext<ContextState>({
  store: rnBoilerplateStore,
  apiClient,
  dropdownAlert: undefined,
  message: undefined,
  theme: "light",
});

export function useReportBack() {
  return useContext(ReportBackContext);
}

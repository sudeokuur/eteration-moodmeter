import React, { useEffect, useRef } from "react";
import CodePush from "react-native-code-push";
import notifee from "@notifee/react-native";

import { useTranslation } from "react-i18next";
import { ActivityIndicator } from "react-native";
import messaging from "@react-native-firebase/messaging";

import { LoaderScreen, Colors, Text, View } from "react-native-ui-lib";

import DropdownAlert from "react-native-dropdownalert";
import { observer } from "mobx-react-lite";
import { rnBoilerplateStore, ReportBackContext, useReportBack, apiClient } from "./ReportBackContext";
import { codePushStatusDidChange } from "../utils/codepush";
import { onDisplayNotification } from "../utils/notifee";

type Props = {
  styles?: object;
  stack?: any;
  children: JSX.Element;
};

const apc: any = apiClient;
apc.rnBoilerplateStore = rnBoilerplateStore;

export function ProviderContents({ children, stack }: Props): JSX.Element {
  const {
    store: { user, isUserSessionStarted, message },
  } = useReportBack();
  const { t } = useTranslation();

  useEffect(() => {
    notifee.onBackgroundEvent(async ({ type, detail }) => {
      console.log(type, detail);
    });
    notifee.onForegroundEvent(async ({ type, detail }) => {
      console.log(type, detail);
    });
    messaging().setBackgroundMessageHandler(async (remoteMessage) => {
      console.log("Message handled in the background!", remoteMessage);
    });
    const unsubscribe = messaging().onMessage(async (remoteMessage) => {
      console.log("A new FCM message arrived!", JSON.stringify(remoteMessage));
    });

    return () => {
      unsubscribe();
    };
  }, []);

  useEffect(() => {
    // user && onSignIn(user);
  }, [user]);

  useEffect(() => {
    onDisplayNotification({ t, undefined, payload: message });
  }, [message, t]);

  useEffect(() => {
    if (!__DEV__) {
      const codePushOptions = {
        updateDialog: {
          appendReleaseDescription: true,
          descriptionPrefix: "\n\nGüncelleme Bilgileri:\n",
        },
        checkFrequency: CodePush.CheckFrequency.ON_APP_RESUME,
      };
      CodePush.sync(codePushOptions, codePushStatusDidChange(DropdownAlert));
    }
    isUserSessionStarted();
  }, []);

  return <>{!!stack && children}</>;
}

export const ReportBackProvider = observer(({ children }: Props): JSX.Element => {
  const dropdownAlert = useRef<any>();
  const { t } = useTranslation();

  const {
    store: { stack, loading, message, theme, overviewList },
  } = useReportBack();

  return (
    <ReportBackContext.Provider
      value={{
        store: rnBoilerplateStore,
        dropdownAlert,
        apiClient,
        message,
        theme,
        overviewList,
      }}
    >
      <ProviderContents stack={stack}>{children}</ProviderContents>
      <DropdownAlert ref={dropdownAlert} />
      {!!loading && (
        <LoaderScreen
          messageStyle={{ color: Colors.white }}
          customLoader={
            <View
              style={{
                backgroundColor: "gray",
                padding: 30,
                borderRadius: 20,
              }}
            >
              <ActivityIndicator color="#ffffff" />
              <Text heading1 white>
                {t("Loading...")}
              </Text>
            </View>
          }
          overlay
        />
      )}
    </ReportBackContext.Provider>
  );
});

import { makeObservable, action, observable } from "mobx";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { Colors, SchemeType } from "react-native-ui-lib";

import { apiClient } from "./ReportBackContext";
import { setStorage } from "../utils/storage";

export enum StackStype {
  Auth = 1,
  App,
  Onboarding,
  CreateUser,
  OnboardingSuccess,
  Wallet,
}

export class ReportBackStore {
  stack = StackStype.Auth;

  user = undefined;

  message = undefined;

  session = undefined;

  error = undefined;

  loading = false;

  theme = "light";

  constructor() {
    makeObservable(this, {
      stack: observable,
      user: observable,
      session: observable,
      message: observable,
      error: observable,
      loading: observable,
      theme: observable,
      setUser: action.bound,
      setStack: action.bound,
      setSession: action.bound,
      setError: action.bound,
      setLoading: action.bound,
      reset: action.bound,
      setMessage: action.bound,
      isUserSessionStarted: action.bound,
      login: action.bound,
      logout: action.bound,
      setTheme: action.bound,
    });
  }

  setLoading(newLoading: any) {
    this.loading = newLoading;
  }

  setUser(newUser: any) {
    this.user = newUser;
  }

  setTheme(theme: SchemeType) {
    Colors.setScheme(theme);
    this.theme = theme;
    setStorage("@theme", theme);
  }

  getUser() {
    return this.user;
  }

  setStack(typ: any) {
    this.stack = typ;
  }

  setSession(newSession: any) {
    if (apiClient && newSession) {
      apiClient.defaults.headers.Authorization = `Bearer ${newSession.token.access_token}`;
      try {
        const jsonValue = JSON.stringify(newSession);
        AsyncStorage.setItem("@x-session", jsonValue);
      } catch (e) {
        console.error(e);
      }
    }
    this.session = newSession;
    this.setUser(newSession.profile);
  }

  restore() {
    try {
      const value = AsyncStorage.getItem("@x-session")
        .then((json: any) => {
          console.log("RESTORED TOKEN", json);

          const x_token = !!json && JSON.parse(json);
          console.log("RESTORED XTOKEN", x_token);

          if (x_token) {
            this.setSession(x_token);
            this.setStack(StackStype.App);
          }
        })
        .catch((e) => {
          console.error(e);
          this.reset();
        });
    } catch (e) {
      console.error(e);
      this.reset();
    }
  }

  setError(newError: any) {
    this.error = newError;
  }

  reset() {
    if (apiClient) {
      apiClient.defaults.headers.Authorization = null;
    }
    AsyncStorage.removeItem("@x-session");

    this.stack = StackStype.Auth;
    this.error = undefined;
    this.session = undefined;
    this.user = undefined;
  }

  setMessage(msg: any) {
    this.message = msg;
  }

  isUserSessionStarted() {
    console.log("USER SESSION CHECK");
    this.restore();
  }

  logout() {
    !!apiClient && apiClient.get("/auth/logout");
    this.reset();
  }

  login(req: any, onSuccess: Function, onFailure: Function) {
    this.error && this.setError(undefined);
    if (req?.customerNumber && req?.pin && req.pin === "123456") {
      const user = {
        name: "Mr Crypto",
        customerNumber: req?.customerNumber,
      };
      const session = { user };
      this.setUser(user);
      this.setSession(session);
      this.setStack(StackStype.App);
      onSuccess(session);
    } else {
      const error = {
        code: 1234,
        message: `The input does not match user ${JSON.stringify(req)}`,
      };
      onFailure(error);
    }
  }
}
// Instantiate the store.

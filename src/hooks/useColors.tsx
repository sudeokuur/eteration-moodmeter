import { useTheme } from '@react-navigation/native';

export function useColors() {
  const { colors } = useTheme();

  return { colors };
}

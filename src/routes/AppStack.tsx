import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { Text } from "react-native-ui-lib";
import { useTranslation } from "react-i18next";
import { getFocusedRouteNameFromRoute } from "@react-navigation/native";
import { Platform } from "react-native";
import { HomeStack } from "../screens/Home/home.stack";
import { IdeasStack } from "../screens/Ideas/ideas.stack";
import { Icon } from "../components/ui";
import { useColors } from "../hooks/useColors";

const Tab = createBottomTabNavigator();

const tabBarIcon = (route: any, focused: boolean, colors: any) => {
  const { name } = route;

  const iconColor = focused ? colors.action : colors.secondaryText;
  let icon = <Icon name="home" fill={iconColor} width="22" height="22" />;
  switch (name) {
    case "markets":
      icon = <Icon name="markets" fill={iconColor} width="22" height="22" />;
      break;
    case "portfolio":
      icon = <Icon name="portfolio" fill={iconColor} width="22" height="22" />;
      break;
    case "orders":
      icon = <Icon name="orders" fill={iconColor} width="22" height="22" />;
      break;
    case "ideas":
      icon = <Icon name="ideas" fill={iconColor} width="22" height="22" />;
      break;
    default:
      break;
  }
  return icon;
};

function AppStack() {
  const { t } = useTranslation();
  const { colors } = useColors();

  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        headerShadowVisible: false,
        headerShown: false,
        tabBarIcon: ({ focused }) => tabBarIcon(route, focused, colors),
        tabBarStyle: {
          backgroundColor: colors.background,
          height: Platform.select({ ios: 92, android: 60 }),
          borderTopWidth: 0,
        },
        tabBarLabel: ({ focused }) => (
          <Text overline1 marginB-8 color={colors.secondaryText}>
            {t(route.name)}
          </Text>
        ),
        headerTitleStyle: { alignSelf: "center" },
      })}
    >
      <Tab.Screen
        name="home"
        component={HomeStack}
        options={({ route }: any) => {
          const routeName = getFocusedRouteNameFromRoute(route);
          let style = {};
          if (routeName === "editOverview") {
            style = {
              tabBarStyle: { display: "none" },
            };
          }
          return style;
        }}
      />
      <Tab.Screen name="ideas" component={IdeasStack} />
    </Tab.Navigator>
  );
}

export default AppStack;

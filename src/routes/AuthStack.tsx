import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import LoginHomeScreen from '../screens/Login/login.home';

const Stack = createNativeStackNavigator();

export function AuthStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen options={{ headerShown: false }} name="loginHome" component={LoginHomeScreen} />
    </Stack.Navigator>
  );
}

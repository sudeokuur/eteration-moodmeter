import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { CreateUserScreen } from '../screens/Onboarding/CreateUserScreen';
import { OnboardingSuccessScreen } from '../screens/Onboarding/OnboardingSuccessScreen';
import { OtpScreen } from '../screens/Login/OtpScreen';

export type OnboardingStackParamList = {
  Onboarding: undefined; // undefined means routes doesn't have params
  OnboardingSuccessScreen: undefined;
  OnboardingOtpScreen: undefined;
};

const Stack = createNativeStackNavigator<OnboardingStackParamList>();

export function OnboardingStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Onboarding"
        component={CreateUserScreen}
        options={{ headerBackTitleVisible: false }} />
      <Stack.Screen
        name="OnboardingSuccessScreen"
        component={OnboardingSuccessScreen}
        options={{ headerBackTitleVisible: false }} />
      <Stack.Screen
        name="OnboardingOtpScreen"
        component={OtpScreen}
        options={{ headerBackTitleVisible: false }} />
    </Stack.Navigator>
  );
}

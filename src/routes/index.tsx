import React from "react";
import { StatusBar } from "react-native";
import { NavigationContainer, NavigationContainerRef } from "@react-navigation/native";
import _ from "lodash";
import { StackStype, useReportBack } from "../context";
import AppStack from "./AppStack";
import { AuthStack } from "./AuthStack";
import { darkTheme, lightTheme } from "../themes/schemes";
import { useColors } from "../hooks/useColors";

export function Router() {
  const routeNameRef = React.useRef<NavigationContainerRef<ReactNavigation.RootParamList>>();
  const navigationRef = React.useRef<NavigationContainerRef<ReactNavigation.RootParamList>>();
  const { colors } = useColors();
  const {
    store: { stack, theme },
  } = useReportBack();

  return (
    <NavigationContainer
      theme={theme === "dark" ? darkTheme : lightTheme}
      ref={navigationRef}
      onReady={() => {
        const nav: any = navigationRef.current;
        routeNameRef.current = nav?.getCurrentRoute().name;
      }}
    >
      <StatusBar backgroundColor={colors.background} barStyle={theme === "dark" ? "light-content" : "dark-content"} />
      {stack === StackStype.Auth && <AuthStack />}
      {stack === StackStype.App && <AppStack />}
      {/* {stack === StackStype.Onboarding && <OnboardingStack />} */}
    </NavigationContainer>
  );
}

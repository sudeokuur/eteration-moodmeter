import React from "react";
import { useTranslation } from "react-i18next";
import _ from "lodash";

import { BaseScreen } from "../../components/ui";
import { Switch, Text, View } from "react-native-ui-lib";
import { useReportBack } from "../../context";

export function DashboardScreen() {
  const { t } = useTranslation();
  const {
    store: { theme, setTheme },
  } = useReportBack();
  return (
    <BaseScreen
      header={{
        title: t("home"),
        showSearchIcon: true,
        showProfileImage: true,
        lightTitle: true,
      }}
    >
      <View margin-16 flex-0 row spread>
        <Text primaryText heading3>
          Dark Theme
        </Text>

        <Switch value={theme === "dark"} onValueChange={() => setTheme(theme === "dark" ? "light" : "dark")} />
      </View>
      <View flex center>
        <Text primaryText heading1>
          {t("welcome")}
        </Text>
      </View>
    </BaseScreen>
  );
}

import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { useTranslation } from "react-i18next";
import { DashboardScreen } from ".";
import { useColors } from "../../hooks/useColors";

const Stack = createNativeStackNavigator();

const headerOptions = (title: any, colors: any): object => ({
  headerShown: false,
  headerBackTitleVisible: false,
  headerTitle: title,
  headerTitleAlign: "center",
  headerStyle: {
    backgroundColor: colors?.background,
  },
  headerTitleStyle: colors.white,
  headerTintColor: colors.white,
});

export function HomeStack() {
  const { colors } = useColors();
  const { t } = useTranslation();

  return (
    <Stack.Navigator screenOptions={{}}>
      <Stack.Screen
        options={{
          ...headerOptions(t("dashboard"), colors),
          headerShown: false,
        }}
        name="dashboard"
        component={DashboardScreen}
      />
    </Stack.Navigator>
  );
}

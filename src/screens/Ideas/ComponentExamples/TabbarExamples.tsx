import { Card } from "react-native-ui-lib";
import React, { useState } from "react";
import { Watchlist } from "../../../components/ui/Watchlist";

export function CustomTabItem() {
  const data = [
    {
      "data": {
        "configDataMap": {
          "defaultWatchList": [
            "XU100",
            "XU030",
            "USDTRY",
            "EURTRY",
            "EURUSD",
            "XAUUSD",
            "GLDGR",
            "GARAN",
            "YKBNK"
          ]
        }
      }
    }

  ]
  return (
    <Watchlist type="square" data={data} />
  )
}
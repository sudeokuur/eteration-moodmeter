import React from 'react';
import { Text, View } from 'react-native-ui-lib';
import { Button } from '../../../components/ui';

export function ButtonExamples() {
  return (
    <View style={{ flex: 1, gap: 50 }}>
      <View style={{ flex: 1, gap: 20 }}>
        <Text primaryText>Success Button Group</Text>
        <Button success label="Button Success" onPress={() => console.log('onPress')} />
        <Button success label="Button Success Disabled" onPress={() => console.log('onPress')} disabled />
        <Button
          success
          label="Button Success With Description"
          onPress={() => console.log('onPress')}
          description="Helper Text"
        />
        <Button
          success
          label="Button Success With Description"
          onPress={() => console.log('onPress')}
          description="Helper Text"
          disabled
        />
      </View>

      <View style={{ flex: 1, gap: 20 }}>
        <Text primaryText>Danger Button Group</Text>
        <Button danger label="Button Danger" onPress={() => console.log('onPress')} />
        <Button danger label="Button Danger Disabled" onPress={() => console.log('onPress')} disabled />
        <Button
          danger
          label="Button Success With Description"
          onPress={() => console.log('onPress')}
          description="Helper Text"
        />
        <Button
          danger
          label="Button Success With Description"
          onPress={() => console.log('onPress')}
          description="Helper Text"
          disabled
        />
      </View>

      <View style={{ flex: 1, gap: 20 }}>
        <Text primaryText>Secondary Button Group</Text>
        <Button label="Button Secondary" onPress={() => console.log('onPress')} secondary size="small" />
        <Button
          label="Button Secondary Disabled"
          onPress={() => console.log('onPress')}
          secondary
          disabled
          size="small"
        />
        <Button
          label="Button Secondary With Icon"
          onPress={() => console.log('onPress')}
          iconName="pencil"
          secondary
          size="small"
        />
        <Button
          label="Button Secondary With Icon"
          onPress={() => console.log('onPress')}
          iconName="pencil"
          secondary
          disabled
          size="small"
        />
      </View>

      <View style={{ flex: 1, gap: 20 }}>
        <Text primaryText>Primary Button Group</Text>
        <Button label="Button Primary" onPress={() => console.log('onPress')} />
        <Button label="Button Outline" onPress={() => console.log('onPress')} outline />
        <Button label="Button Outline Secondary" onPress={() => console.log('onPress')} outline secondary />
      </View>

      <View style={{ flex: 1, gap: 20 }}>
        <Text>Icon Button Group</Text>
        <Button label="Button Primary With Icon" onPress={() => console.log('onPress')} iconName="pencil" />
        <Button label="Button Outline With Icon" onPress={() => console.log('onPress')} outline iconName="pencil" />
        <Button
          label="Button Outline Secondary With Icon"
          onPress={() => console.log('onPress')}
          outline
          secondary
          iconName="pencil"
        />
      </View>

      <View style={{ flex: 1, gap: 20 }}>
        <Text primaryText>Disabled Icon Button Group</Text>
        <Button
          label="Button Primary Disabled With Icon"
          onPress={() => console.log('onPress')}
          disabled
          iconName="pencil"
        />
        <Button
          label="Button Outline Disabled With Icon"
          onPress={() => console.log('onPress')}
          disabled
          outline
          iconName="pencil"
        />
        <Button
          label="Button Outline Secondary Disabled With Icon"
          onPress={() => console.log('onPress')}
          disabled
          outline
          secondary
          iconName="pencil"
        />
      </View>

      <View style={{ flex: 1, gap: 20 }}>
        <Text primaryText>Disabled Button Group</Text>
        <Button label="Button Primary Disabled" onPress={() => console.log('onPress')} disabled />
        <Button label="Button Outline Disabled" onPress={() => console.log('onPress')} disabled outline />
        <Button
          label="Button Outline Secondary Disabled"
          onPress={() => console.log('onPress')}
          disabled
          outline
          secondary
        />
      </View>
    </View>
  );
}

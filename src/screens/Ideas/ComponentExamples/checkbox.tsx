import React, { useState } from 'react';
import { Card, Text, View } from 'react-native-ui-lib';
import { Checkbox, StockIcon } from '../../../components/ui';

export function CheckboxExamples() {
  const [checkbox, setCheckBox] = useState(false);
  return (
    <Card margin-2>
      {/* tüm propslarına erişim vardır */}
      <Text primaryText heading6>
        No Label
      </Text>
      <View marginV-8>
        <Checkbox value={checkbox} onValueChange={() => setCheckBox(!checkbox)} />
      </View>

      <Text primaryText heading6>
        With Label
      </Text>
      <View marginV-8>
        <Checkbox type="label" label="Ozerden Plastik" value={checkbox} onValueChange={() => setCheckBox(!checkbox)} />
      </View>

      <Text primaryText heading6>
        With Stock Icon
      </Text>
      <View marginV-8>
        <Checkbox
          type="description"
          secondIcon={<StockIcon name={'Thyao'} size={32} />}
          description="Turk Hava Yolları A.S."
          label="THYAO"
          value={checkbox}
          onValueChange={() => setCheckBox(!checkbox)}
        />
      </View>
    </Card>
  );
}

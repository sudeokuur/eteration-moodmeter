import React, { useState } from 'react';
import { View } from 'react-native-ui-lib';
import { TextField, CounterInput, AmountInput, SearchInput } from '../../../components/ui';

export function TextFieldExamples() {
  const [val1, setVal1] = useState();
  const [val2, setVal2] = useState();
  const [val3, setVal3] = useState();
  const [val4, setVal4] = useState('12');
  const [val5, setVal5] = useState('1264565.465');

  return (
    <View>
      <SearchInput placeholder="Search Sembol" search />

      <TextField
        label="label"
        placeholder="Placeholder"
        onChangeText={(e: any) => setVal1(e)}
        validate={['required', (value: string | any[]) => value.length > 6]}
        validationMessage={['Field is required', 'Password is too short']}
        showCharCounter
        maxLength={30}
        value={val1}
        info="Information"
      />

      <TextField
        label="No Validation"
        placeholder="Placeholder"
        onChangeText={(e: any) => setVal2(e)}
        // validate={['required']}
        // validationMessage={['Field is required']}
        showCharCounter
        maxLength={30}
        value={val2}
        info="Information"
      />

      <TextField
        label="No Counter-Info"
        placeholder="Placeholder"
        onChangeText={(e: any) => setVal3(e)}
        // showCharCounter
        // validate={['required', (value: string | any[]) => value.length > 6]}
        // validationMessage={['Field is required', 'Password is too short']}
        value={val3}
      />

      <CounterInput label="Quantity" onChangeText={(e: any) => console.log(e)} />

      <CounterInput label="Price" onChangeText={(e: any) => setVal4(e)} value={val4} increaseRatio={1.5} />

      <AmountInput
        label="Amount"
        onChangeText={(e: any) => setVal5(e)}
        value={val5}
        info="Amount information"
        currency="TL"
        validate={['required']}
        validationMessage={['Field is required']}
      />

      <AmountInput
        label="Amount"
        onChangeText={(e: any) => console.log(e)}
        info="Amount information"
        currency="TL"
        validate={['required']}
        validationMessage={['Field is required']}
      />
    </View>
  );
}

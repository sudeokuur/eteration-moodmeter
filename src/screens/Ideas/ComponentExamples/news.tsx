import React from "react";
import LinearGradient from "react-native-linear-gradient";
import { Card, Colors, Text, View } from "react-native-ui-lib";
import { Icon, StockIcon } from "../../../components/ui";
import { useReportBack } from "../../../context";

export default function NewsExample() {
  const {
    store: { theme },
  } = useReportBack();
  return (
    <>
      <LinearGradient
        // style={styles.container}
        start={{ x: 0, y: 0 }}
        end={{ x: 0.8, y: 2.5 }}
        locations={[0.3, 1]}
        style={{ borderRadius: 12, padding: 8 }}
        colors={theme === "dark" ? [Colors.secondaryBgStartLight, Colors.cardBgEndDark] : [Colors.white, Colors.white]}
      >
        <View margin-6>
          <View centerV row>
            <Text body2 secondaryText marginR-10>
              June 13 -
            </Text>
            <Text body2 secondaryText marginR-10>
              11:00 -
            </Text>
            <Text body2 secondaryText marginR-10>
              Symbol news
            </Text>
            <Icon name="apple" width="24" height="24"></Icon>
          </View>
          <Text body1 primaryText marginT-6>
            During the e-transaction, the process was interrupted and the single price order collection started.
          </Text>
        </View>
      </LinearGradient>

      <View marginV-8>
        <LinearGradient
          // style={styles.container}
          start={{ x: 0, y: 0 }}
          end={{ x: 0.8, y: 2.5 }}
          locations={[0.3, 1]}
          style={{ borderRadius: 12, padding: 16 }}
          colors={
            theme === "dark" ? [Colors.secondaryBgStartLight, Colors.cardBgEndDark] : [Colors.white, Colors.white]
          }
        >
          <View marginV-8 spread row>
            <StockIcon name="thyao" size={36} />
            <View centerH>
              <Text body2 secondaryText marginR-10>
                Today
              </Text>
              <Text body2 secondaryText marginR-10>
                10:05
              </Text>
            </View>
          </View>

          <Text overline>Dividend payment has been made for the Garan in your account number 150887-100</Text>
        </LinearGradient>
      </View>

      <View marginV-8>
        <LinearGradient
          // style={styles.container}
          start={{ x: 0, y: 0 }}
          end={{ x: 0.8, y: 2.5 }}
          locations={[0.3, 1]}
          style={{ borderRadius: 12, padding: 16 }}
          colors={
            theme === "dark" ? [Colors.secondaryBgStartLight, Colors.cardBgEndDark] : [Colors.white, Colors.white]
          }
        >
          <View marginV-8 spread row>
            <StockIcon name="thyao" size={36} />
            <Text marginH-16 flex-5 overline>
              Dividend payment has been made for the Garan in your account number 150887-100
            </Text>
            <Text body2 secondaryText marginR-10>
              10:05
            </Text>
          </View>
        </LinearGradient>
      </View>
    </>
  );
}

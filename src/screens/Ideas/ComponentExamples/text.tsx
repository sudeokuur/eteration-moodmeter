import React from 'react';
import { Card, Text } from 'react-native-ui-lib';

export function TextExamples() {
  return (
    <Card margin-2>
      <Text heading1 primaryText>
        Benton Sans BBVA / Light / 28 / 40
      </Text>
      <Text heading2 primaryText>
        Benton Sans BBVA / Light / 22 / 32
        {' '}
      </Text>
      <Text heading3 primaryText>
        Benton Sans BBVA / Medium / 18 / 24
        {' '}
      </Text>
      <Text heading4 primaryText>
        Benton Sans BBVA / Medium / 16 / 24
        {' '}
      </Text>
      <Text heading5 primaryText>
        Benton Sans BBVA / Book / 16 / 24
        {' '}
      </Text>
      <Text heading6 primaryText>
        Benton Sans BBVA / Medium / 14 / 24
        {' '}
      </Text>
      <Text heading7 primaryText>
        Benton Sans BBVA / Medium / 12 / 16
        {' '}
      </Text>
      <Text overline1 primaryText>
        Benton Sans BBVA / Book / 11 / 12
        {' '}
      </Text>
      <Text overline2 primaryText>
        Benton Sans BBVA / Medium / 11 / 16
        {' '}
      </Text>
      <Text overline3 primaryText>
        Benton Sans BBVA / Book / 12 / 16
        {' '}
      </Text>
      <Text button1 primaryText>
        Benton Sans BBVA / Medium / 12 / 16
        {' '}
      </Text>
      <Text button2 primaryText>
        Benton Sans BBVA / Book / 12 / 16
        {' '}
      </Text>
      <Text digits1 primaryText>
        Benton Sans BBVA / Light / 32 / 40
        {' '}
      </Text>
      <Text digits2 primaryText>
        Benton Sans BBVA / Book / 24 / 40
        {' '}
      </Text>
      <Text digits3 primaryText>
        Benton Sans BBVA / Book / 16 / 20
        {' '}
      </Text>
      <Text digits4 primaryText>
        Benton Sans BBVA / Medium / 14 / 20
        {' '}
      </Text>
      <Text initials primaryText>
        Benton Sans BBVA / Medium / 32 / 40
        {' '}
      </Text>
    </Card>
  );
}

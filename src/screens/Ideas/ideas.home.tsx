import React from 'react';
import { useTranslation } from 'react-i18next';
import { Text, TouchableOpacity, View } from 'react-native-ui-lib';
import { useNavigation } from '@react-navigation/native';

import { BaseScreen } from '../../components/ui';

export function IdeasHomeScreen() {
  const { t } = useTranslation();
  const navigation = useNavigation();
  return (
    <BaseScreen
      header={{
        title: t('ideas'),
        showSearchIcon: true,
        showProfileImage: true,
      }}>
      <View center>
        <TouchableOpacity onPress={() => navigation.navigate('training' as never)}>
          <Text heading2 color="red">
            Çalışma Sayfasına Geçiş
          </Text>
        </TouchableOpacity>
        <Text heading5>Ideas</Text>
      </View>
    </BaseScreen>
  );
}

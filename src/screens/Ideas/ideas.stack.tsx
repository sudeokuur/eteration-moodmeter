import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { IdeasHomeScreen } from './ideas.home';
import { IdeasTrainig } from './ideas.training';

const Stack = createNativeStackNavigator();

export function IdeasStack() {
  return (
    <Stack.Navigator
      initialRouteName="training"
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name="ordersHome" component={IdeasHomeScreen} />
      <Stack.Screen name="training" component={IdeasTrainig} />
    </Stack.Navigator>
  );
}

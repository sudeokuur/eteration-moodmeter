import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import { ScrollView } from "react-native-gesture-handler";
import { ExpandableSection, Text } from "react-native-ui-lib";
import { BaseScreen } from "../../components/ui";
import { ButtonExamples, TextFieldExamples, TextExamples } from "./ComponentExamples";
import { CheckboxExamples } from "./ComponentExamples/checkbox";

export function IdeasTrainig() {
  const { t } = useTranslation();
  const [textsExpanded, setTextsExpanded] = useState(false);
  const [buttonsExpanded, setButtonsExpaned] = useState(false);
  const [inputsExpanded, setInputsExpaned] = useState(false);
  const [checkbox, setCheckbox] = useState(false);

  return (
    <BaseScreen
      header={{
        title: t("Ideas"),
        showSearchIcon: true,
        showProfileImage: true,
      }}
    >
      <ScrollView
        keyboardShouldPersistTaps="handled"
        contentContainerStyle={{ flexGrow: 1, paddingBottom: 80, marginHorizontal: 16 }}
        showsVerticalScrollIndicator={false}
      >
        <ExpandableSection
          expanded={textsExpanded}
          sectionHeader={
            <Text digits2 primaryText>
              Texts
            </Text>
          }
          onPress={() => setTextsExpanded(!textsExpanded)}
        >
          <TextExamples />
        </ExpandableSection>
        <ExpandableSection
          expanded={checkbox}
          sectionHeader={
            <Text digits2 primaryText>
              Checkbox
            </Text>
          }
          onPress={() => setCheckbox(!checkbox)}
        >
          <CheckboxExamples />
        </ExpandableSection>

        <ExpandableSection
          expanded={buttonsExpanded}
          sectionHeader={
            <Text digits2 primaryText>
              Buttons
            </Text>
          }
          onPress={() => setButtonsExpaned(!buttonsExpanded)}
        >
          <ButtonExamples />
        </ExpandableSection>

        <ExpandableSection
          expanded={inputsExpanded}
          sectionHeader={
            <Text digits2 primaryText>
              Inputs
            </Text>
          }
          onPress={() => setInputsExpaned(!inputsExpanded)}
        >
          <TextFieldExamples />
        </ExpandableSection>
      </ScrollView>
    </BaseScreen>
  );
}

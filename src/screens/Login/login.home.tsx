import React, { useEffect, useState } from "react";
import { StyleSheet ,Text,Image,TextInput, useColorScheme, ViewStyle} from "react-native";
import { View, Typography, TextField, ColorPicker } from "react-native-ui-lib";

import { SafeAreaView } from "react-native-safe-area-context";
import { StackStype } from "../../context/ReportBackStore";
import { useReportBack } from "../../context";

import { Button, Icon} from "../../components/ui";
import { useColors } from "../../hooks/useColors";
//import { TextInput } from "react-native-gesture-handler";

import Logo from '../../../assets/images/Eteration-logo.png' ;
import { tr } from "../../translations";
import { colorsPalette } from "react-native-ui-lib/src/style/colorsPalette";
import { ScrollView } from "react-native-gesture-handler";
import colorObject from "react-native-ui-lib/src/style/colors";

export function LoginHomeScreen() {
  const { colors } = useColors();
  const [mail,setMail] = useState('');
  const [password,setPassword] = useState('');
  
  
  const {
    store: { setUser, setStack },
  } = useReportBack();

  const doLogin = () => {
    setStack(StackStype.App);
  };
  Typography.loadTypographies({
    tf1: {fontSize: 58, fontWeight: '300', lineHeight: 80},
    tf2: {fontSize: 46, fontWeight: '300', lineHeight: 64},
  });
  
  

  return (
    <ScrollView showsVerticalScrollIndicator={false}>
         <SafeAreaView style={styles.login}>
      <View>
        <Image style={styles.image} source={Logo} resizeMode="contain"/>
        <View>
          <Text style={{color:'black',fontSize:32,fontWeight:600}}>Let's Sign You In</Text>
          <Text style={{color:'black',fontSize:20,fontWeight:400}}>Welcome back, you've been missed!</Text>
        </View>

        <View style={styles.inputContainer}>

        <TextInput  placeholder={"Enter your e-mail."} onChangeText={(value)=>setMail(value)} keyboardType="email-address"/>

        </View>

        <View style={styles.inputContainer}>
        <TextInput placeholder={"Enter your password."} secureTextEntry={true} onChangeText={(value)=>setPassword(value)}/> 
        </View>

        <Button label="SIGN IN" onPress={doLogin} backgroundColor={colorObject.getHexString("FE6601")}/>
        <View style={{flex:1,flexDirection:"column",alignItems:"center",marginTop:10,marginBottom:25}}>
           <Text style={{color:'black',fontSize:18 , marginRight:5}}>Don't have an account?</Text>
           <Text style={{color:'black',fontWeight:"800",fontSize:18}}>Sign up</Text>
        </View>
        <Button label="Connect with Google" onPress={doLogin} backgroundColor={colorObject.getHexString("FE6601")}/>
        
      
      </View>
    </SafeAreaView>
    </ScrollView>
   
  );
}


const styles = StyleSheet.create({
  login: {
    flex: 1,
    alignItems: "center",
    justifyContent:"flex-start",
    margin: 10,
    backgroundColor:'white'
  },
  image:{
    height:200,
    width:300
  },
  inputContainer:{
    width: 350,
    borderColor: 'grey',
    paddingHorizontal:5,
    borderBottomColor:'grey',
    borderBottomWidth:1,
    marginVertical:15
  }
});

export default LoginHomeScreen;

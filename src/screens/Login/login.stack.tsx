import { createNativeStackNavigator } from '@react-navigation/native-stack';
import React from 'react';
import LoginHomeScreen from './login.home';

const Stack = createNativeStackNavigator();

export function LoginStack() {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="loginHome" component={LoginHomeScreen} />
    </Stack.Navigator>
  );
}

import React from "react";
import { StyleSheet } from "react-native";
import { View, Text } from "react-native-ui-lib";
import { useTranslation } from "react-i18next";
import { useReportBack } from "../../context/ReportBackContext";

export function PlaceholderHomeScreen() {
  const { t } = useTranslation();
  const {
    store: { user },
  } = useReportBack();

  return (
    <View style={styles.container}>
      <Text>{user?.name}</Text>
      <Text>{t("placeholder home")}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: { flex: 1, justifyContent: "center", alignItems: "center" },
});

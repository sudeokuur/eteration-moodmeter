import { createNativeStackNavigator } from '@react-navigation/native-stack';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { PlaceholderHomeScreen } from './placeholder.home';
import colors from '../../themes/colors';

const Stack = createNativeStackNavigator();

const headerOptions = (title: any, headerShown?: boolean): object => ({
  headerShown: headerShown ?? true,
  headerBackTitleVisible: false,
  headerTitle: title,
  headerTitleAlign: 'center',
  headerStyle: {
    backgroundColor: colors.white,
  },
  headerTitleStyle: { color: colors.black },
  headerTintColor: colors.yellow,
});

export function HomeStack() {
  const { t } = useTranslation();

  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen
        options={{ ...headerOptions(t('ordersHome')) }}
        name="ordersHome"
        component={PlaceholderHomeScreen} />
    </Stack.Navigator>
  );
}

import { Colors, Typography, ThemeManager } from 'react-native-ui-lib';

ThemeManager.setComponentTheme('Button', (props: {
  outline: boolean; secondary: boolean; disabled: boolean; pressed: boolean; size: string;
}) => {
  let outlineColor;
  let buttonPaddingVertical;
  const disabledBackgroundColor = Colors.$btnPrimaryDisabled;
  let labelColor = props.pressed === true ? Colors.$btnPrimaryPressedText : Colors.$btnPrimaryText;
  let backgroundColor = Colors.$btnPrimary;
  let activeBackgroundColor = Colors.$btnPrimaryPressedBackground;

  if (props.outline) {
    backgroundColor = 'transparent';
    activeBackgroundColor = Colors.$btnOutlinePressedBackground;
    outlineColor = Colors.$btnPrimary;
    labelColor = props.pressed === true ? Colors.$btnPrimaryPressedText : Colors.$btnOutlineText;

    if (props.disabled) {
      labelColor = Colors.$btnOutlineDisabledText;
    }

    if (props.secondary) {
      activeBackgroundColor = 'transparent';
      outlineColor = props.pressed === true ? Colors.$btnOutlineText : Colors.$btnOutlineSecondary;
      labelColor = Colors.$btnOutlineText;

      if (props.disabled) {
        labelColor = Colors.$btnOutlineDisabledText;
        outlineColor = Colors.$btnPrimaryDisabled;
      }
    }
  } else if (props.secondary) {
    backgroundColor = Colors.$btnSecondary;
    activeBackgroundColor = Colors.$btnSecondaryPressed;
    labelColor = props.pressed === true ? Colors.$btnSecondaryPressedText : Colors.$btnSecondaryText;

    if (props.disabled) {
      labelColor = Colors.$btnSecondaryDisabledText;
      backgroundColor = Colors.$btnSecondaryDisabled;
    }
  }

  switch (props.size) {
    case 'xsmall':
      buttonPaddingVertical = 4;
      break;
    case 'small':
      buttonPaddingVertical = 8;
      break;
    case 'medium':
      buttonPaddingVertical = 12;
      break;
    default:
      buttonPaddingVertical = 16;
      break;
  }

  return {
    activeBackgroundColor,
    backgroundColor,
    disabledBackgroundColor,
    outlineColor,
    borderRadius: 8,
    labelStyle: { ...Typography.button1, color: labelColor },
    style: {
      paddingVertical: buttonPaddingVertical,
    },
  };
});

import { ThemeManager } from 'react-native-ui-lib';

ThemeManager.setComponentTheme('Text', (props: any, _context: any) => {
  const {
    heading1,
    heading2,
    heading3,
    heading4,
    heading5,
    heading6,
    heading7,
    body1,
    body2,
    body3,
    overline1,
    overline2,
    overline3,
    button1,
    button2,
    digits1,
    digits2,
    digits3,
    digits4,
    initials,
  } = props;
  const textStyle: any = {
    // style: {
    //   color: Colors.primaryText,
    // },
  };
  if (heading1) {
    textStyle.heading1 = true;
  }
  if (heading2) {
    textStyle.heading2 = true;
  }
  if (heading3) {
    textStyle.heading3 = true;
  }
  if (heading4) {
    textStyle.heading4 = true;
  }
  if (heading5) {
    textStyle.heading5 = true;
  }
  if (heading6) {
    textStyle.heading6 = true;
  }
  if (heading7) {
    textStyle.heading7 = true;
  }
  if (body1) {
    textStyle.body1 = true;
  }
  if (body2) {
    textStyle.body2 = true;
  }
  if (body3) {
    textStyle.body3 = true;
  }
  if (overline1) {
    textStyle.overline1 = true;
  }
  if (overline2) {
    textStyle.overline2 = true;
  }
  if (overline3) {
    textStyle.overline3 = true;
  }
  if (button1) {
    textStyle.button1 = true;
  }
  if (button2) {
    textStyle.button2 = true;
  }
  if (digits1) {
    textStyle.digits1 = true;
  }
  if (digits2) {
    textStyle.digits2 = true;
  }
  if (digits3) {
    textStyle.digits3 = true;
  }
  if (digits4) {
    textStyle.digits4 = true;
  }
  if (initials) {
    textStyle.initials = true;
  }
  return textStyle;
});

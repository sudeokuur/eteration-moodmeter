/* eslint-disable no-nested-ternary */
import { Colors, ThemeManager, Typography } from 'react-native-ui-lib';
import { Platform } from 'react-native';

ThemeManager.setComponentTheme(
  'Incubator.TextField',
  (props: { isValid: boolean; isFocused: boolean; enableErrors: boolean; search: boolean }) => {
    const TOP_SIZE = Platform.select({ ios: -79, android: -85 });
    const fieldStyle = props.search ?
      {
        paddingBottom: 12,
        paddingLeft: 24,
        paddingRight: 54,
        borderBottomWidth: 1,
        borderBottomColor: Colors.searchBorderWidth,
      }
      : {
        borderWidth: 1,
        borderColor: props.isFocused
          ? Colors.$inputFocusedBorderColor
          : props.isValid
            ? Colors.$inputBorderColor
            : Colors.$inputBorderColorError,
        borderRadius: 8,
        paddingHorizontal: 16,
        paddingVertical: 14,
      };

    return {
      containerStyle: {
        position: 'relative',
      },
      fieldStyle,
      charCounterStyle: {
        ...Typography.body3,
        position: 'absolute',
        top: TOP_SIZE,
        right: 0,
        color: Colors.$inputCounterColor,
      },
      color: Colors.$inputTextColor,
      labelColor: Colors.$inputLabelColor,
      placeholderTextColor: Colors.$inputPlaceHolderColor,
      labelStyle: {
        ...Typography.body2,
        marginBottom: 8,
      },
    };
  },
);

export default {
  // ==========DARK THEME COLORS==========

  // Background = Bg
  // Gradient = Gr
  // ..Dark = Dark Theme
  // ..80Dark = %80 Opacity
  // Herhangi bir sayi yoksa %100 Opacity

  // Main Colors

  actionDark: '#2DCCCD', // Turkuaz- butonlarda ve text butonlardaki ana renk
  action10Dark: 'rgba(45, 204, 205, 0.1)', // Turkuaz %10 Opacity- secili sembol list background
  action20Dark: 'rgba(45, 204, 205, 0.2)', // Turkuaz %20 Opacity
  action50Dark: 'rgba(45, 204, 205, 0.5)', // Turkuaz %50 Opacity- Select new symbol to add ekraninda- Tik
  labelGrStartDark: '#5BBEFF', // Gradient Label Start
  labelGrEndDark: '#5AC4C4', // Gradient Label End
  garantiMainDark: '#15181F', // Garanti Main Color siyah- icon ici yazilar

  // Background Colors

  primaryBgDark: '#040B2F', // Arka Plan
  darkBlueGr40StartDark: 'rgba(23, 39, 96, 0.4)', // Gradient Dark Blue %40 Opacity
  darkBlueGr40EndDark: 'rgba(28, 39, 96, 0.4)', // Gradient Dark Blue %40 Opacity
  menuBg10Dark: 'rgba(20, 100, 165, 0.1)', // tabbar menu arka plan

  // Total Equity Value gorunmeyen kisimlar icin

  cardBgStartDark: '#004481',
  cardBgEndDark: '#8263D9',
  cardBgStart80Dark: 'rgba(0, 68, 129, 0.8)', // Dashboard Notifications card new notification -start-
  cardBgEnd80Dark: 'rgba(130, 99, 217, 0.8)', // Dashboard Notifications card new notification -end-
  cardBgStart50Dark: 'rgba(0, 68, 129, 0.5)', // Gizli total equity value gorunmeyen kisimlar icin
  cardBgEnd50Dark: 'rgba(130, 99, 217, 0.5)', // Gizli total equity value gorunmeyen kisimlar icin
  cardBgStart30Dark: 'rgba(0, 68, 129, 0.3)', // Gizli total equity value gorunmeyen kisimlar icin
  cardBgEnd30Dark: 'rgba(130, 99, 217, 0.3)', // Gizli total equity value gorunmeyen kisimlar icin

  clickableBlockBgStartDark: 'rgba(0, 68, 129, 0.45)', // Notifications card gradient start
  clickableBlockBgEndDark: 'rgba(130, 99, 217, 0.45)', // Notifications card gradient end

  // Text Colors
  primaryTextDark: '#FFFFFF', // Primary Text Beyaz Ana Renk
  secondaryText64Dark: 'rgba(255, 255, 255, 0.64)', // Secondary Text %64 Opacity Gri Renk

  // Activity Colors

  greenStatusDark: '#45E46B', // Green Status Total Equity alaninda + bakiye ikon ve yuzdelik icin
  greenButtonDark: '#2D8742', // Green Button== swipe ile acilan buy arka rengi
  greenButton30Dark: 'rgba(45, 135, 66, 0.3)', //
  disabledGreenButtonDark: 'rgba(18, 102, 55, 0.5)', //
  darkGreenDark: '##183B39', // Dark Green
  blueDark: '##4169CF', // Blue Button== swipe ile acilan alert arka rengi
  blue20Dark: 'rgba(65, 105, 207, 0.2)', // Blue %20 Opacity
  blue50Dark: 'rgba(65, 105, 207, 0.5)', // Blue %50 Opacity Notification center Turn On==Toggle Switch background mavi
  darkBlue20Dark: 'rgba(38, 77, 135, 0.2)', // Dark Blue %20 Opacity
  darkBlue40Dark: 'rgba(38, 77, 135, 0.4)', // Dark Blue %40 Opacity
  redStatusDark: '#FF7461', // Red Status Total Equity alaninda - bakiye ikon ve yuzdelik icin
  redStatus80Dark: 'rgba(255, 116, 97, 0.8)', //
  redButtonDark: '#982E20', // Red Button ===swipe ile acilan sell arka rengi
  redButton30Dark: 'rgba(152, 46, 32, 0.3)', //
  disabledRedButtonDark: '#572222', //
  darkRedDark: '#412A3A', // Dark Red
  yellowStatusDark: '#F8CD51', // Yellow Status Total Equity alaninda + bakiye ikon ve yuzdelik icin

  // Other Colors

  blueVioletGrStartDark: '#2784D8', //
  blueVioletGrEndDark: '#9378E7', //
  blueVioletGrStart30Dark: 'rgba(39, 132, 216, 0.3)', //
  blueVioletGrEnd30Dark: 'rgba(147, 120, 231, 0.3)', //
  iconGrStartDark: '#1F82DB', // Notification center new symbol bg
  iconGrEndDark: '#9374EA', // Notification center new symbol bg
  violetDark: '#9BB6FD', // View Type List secenegindeki background
  blueTurquoiseGrStartDark: '#04EEEF', //
  blueTurquoiseGrEndDark: '#43B1FF', //

  // White Colors
  whiteDark: '#FFFFFF',
  white30Dark: 'rgba(255, 255, 255, 0.3)', // Toggle Switch background gri- watchlist hamburger icon
  white20Dark: 'rgba(255, 255, 255, 0.2)', // Select new symbol to add ekraninda- X iconu background
  blockBg8Dark: '#181E3D',
  blockBg4Dark: 'rgba(255, 255, 255, 0.04)',
  tabBgGrStart8Dark: 'rgba(255, 255, 255, 0.08)',
  tabBgGrEnd8Dark: 'rgba(255, 255, 255, 0)',

  // GRAPH COLORS

  // Top 5 Buyers
  celadonDark: '#AFEFBD', // Celadon
  middleGreenDark: '#41A25C', // Middle Green
  darkSpringGreenDark: '#126637', // Dark Spring Green
  lightSeaGreenDark: '#26B9B0', // Light Sea Green
  darkSeaGreenDark: '#0D5B6C', // Dark Sea Green
  darkGreenGreyDark: '#638593', // Dark Green Grey

  // Top 5 Sellers

  blondDark: '#FFF3B6', // Blond
  deepSaffronDark: '#FF9F2E', // Deep Saffron
  metalicOrangeDark: '#D86008', // Metalic Orange
  sweetBrownDark: '#A23333', // Sweet Brown
  brownDark: '#572222', // Brown
  darkRedGreyDark: '#7A657D', // Dark Red Grey

  // Portfolio Loss

  darkSalmonDark: '#E46B6B', // Dark Salmon
  darkPastelRedDark: '#FF7461', // Dark Pastel Red
  firebrickDark: '#982E20', // Firebrick
  orangeDark: '#FF3B30', // Orange Red
  // brownDark: '#572222',  -> Top 5 Sellers alaninda eklendi
  // darkRedGreyDark: '#7A657D', -> Top 5 Sellers alaninda eklendi

  // Portfolio Account Composition Custodian Analysis

  elektricBlueDark: '#7FE3EB', // Elektric Blue
  vividBlueDark: '#4CABFF', // Vivid Blue
  brightNavyDark: '#3160D8', // Bright Navy
  lavenderDark: '#A27CF2', // Lavender
  indigoDark: '#7027B9', // Indigo
  darkBlueGreyDark: '#656E98', // Dark Blue Grey

  // Foreign Currency Information

  // middleGreenDark: '#41A25C', // -> Top 5 Buyers alaninda eklendi
  greenShadowStart20Dark: 'rgba(65, 162, 92, 0.2)', // Green Shadow
  greenShadowEndDark: 'rgba(65, 162, 92, 0)', // Green Shadow
  malibuDark: '#4CC9FF', // Malibu
  blueShadowStart20Dark: 'rgba(76, 201, 255, 0.2)', // Blue Shadow
  blueShadowEndDark: 'rgba(76, 201, 255, 0)', // Blue Shadow
  // lavenderDark: '#A27CF2', // -> Portfolio Account Composition Custodian Analysis alaninda eklendi
  violettShadowStart20Dark: 'rgba(162, 124, 242, 0.2)', // Violet Shadow
  violettShadowEndDark: 'rgba(162, 124, 242, 0)', // Violet Shadow

  // Symbol Details

  // actionDark: '#FF3B30', // -> Main Colors alaninda eklendi
  turquoiseShadowStart30Dark: 'rgba(45, 204, 205, 0.3)', // Turquoise Shadow
  turquoiseShadowEndDark: 'rgba(45, 204, 205, 0)', // Turquoise Shadow

  // Income Statement

  // electricBlueDark: '#7FE3EB', // Electric Blue
  // brightNavyDark: '#3160D8', // Bright Navy
  // darkBlueGreyDark: '#656E98', // Dark Blue Grey

  // Cash Flow

  // lavanderDark: '#A27CF2', // Lavander
  // indigoDark: '#7027B9', // Indigo
  cosmicCobaltDark: '#303087', // Cosmic Cobalt

  // Finalcial Evaluation Laverage

  // blondDark: '#FFF3B6', // Blond
  // deepSaffronDark: '#FF9F2E', // Deep Saffron
  // middleGreenDark: '#41A25C', // Middle Green

  // Multiples Ratios

  greenOrangeGrStartDark: 'rgba(64, 180, 87, 1)', // Green Orange Gradient Start
  greenOrangeGrMidDark: 'rgba(214, 187, 50, 1)', // Green Orange Gradient End
  greenOrangeGrEndDark: 'rgba(246, 70, 70, 1)', // Green Orange Gradient End

  orangeGreenGrStartDark: 'rgba(246, 70, 70, 1)', // Orange Green Gradient Start
  orangeGreenGrMidDark: 'rgba(214, 187, 50, 1)', // Orange Green Gradient Mid
  orangeGreenGrEndDark: 'rgba(64, 180, 87, 1)', // Orange Green Gradient End

  // WATCLİST 

};

import { Colors, Typography, Spacings, ThemeManager, Assets } from 'react-native-ui-lib';

// Components Theme
import './components/Button';
import './components/Text';
import './components/TextField';

import darkColors from './darkColors';
import lightColors from './lightColors';

import { darkTheme, lightTheme } from './schemes';

Assets.loadAssetsGroup('logos', {});

Colors.loadColors({
  ...darkColors,
  ...lightColors,
});

Colors.loadSchemes({
  light: lightTheme.colors,
  dark: darkTheme.colors,
});

Colors.setScheme('light');

export const fonts = {
  bold: 'BBVABentonSans-Bold',
  book: 'BBVABentonSans-Book',
  bookItalic: 'BBVABentonSans-BookItalic',
  light: 'BBVABentonSansLight-Regular',
  medium: 'BBVABentonSansLight-Bold',
  mediumItalic: 'BBVABentonSansLight-Italic',
};

Typography.loadTypographies({
  heading1: { fontFamily: fonts.light, fontSize: 28, lineHeight: 40, fontWeight: '300' },
  heading2: { fontFamily: fonts.light, fontSize: 22, lineHeight: 32, fontWeight: '300' },
  heading3: { fontFamily: fonts.medium, fontSize: 18, lineHeight: 24, fontWeight: '500' },
  heading4: { fontFamily: fonts.medium, fontSize: 16, lineHeight: 24, fontWeight: '500' },
  heading5: { fontFamily: fonts.book, fontSize: 16, lineHeight: 24, fontWeight: '400' },
  heading6: { fontFamily: fonts.medium, fontSize: 14, lineHeight: 24, fontWeight: '500' },
  heading7: { fontFamily: fonts.medium, fontSize: 12, lineHeight: 16, fontWeight: '500' },
  body1: { fontFamily: fonts.book, fontSize: 14, lineHeight: 20, fontWeight: '400' },
  body2: { fontFamily: fonts.book, fontSize: 12, lineHeight: 16, fontWeight: '400' },
  body3: { fontFamily: fonts.book, fontSize: 11, lineHeight: 16, fontWeight: '400' },
  overline1: { fontFamily: fonts.book, fontSize: 11, lineHeight: 12, fontWeight: '400' },
  overline2: { fontFamily: fonts.medium, fontSize: 11, lineHeight: 16, fontWeight: '500' },
  overline3: { fontFamily: fonts.book, fontSize: 12, lineHeight: 16, fontWeight: '400' },
  button1: { fontFamily: fonts.medium, fontSize: 12, lineHeight: 16, fontWeight: '500' },
  button2: { fontFamily: fonts.book, fontSize: 12, lineHeight: 16, fontWeight: '400' },
  digits1: { fontFamily: fonts.light, fontSize: 32, lineHeight: 40, fontWeight: '300' },
  digits2: { fontFamily: fonts.book, fontSize: 24, lineHeight: 40, fontWeight: '300' },
  digits3: { fontFamily: fonts.book, fontSize: 16, lineHeight: 20, fontWeight: '400' },
  digits4: { fontFamily: fonts.medium, fontSize: 14, lineHeight: 20, fontWeight: '500' },
  initials: { fontFamily: fonts.medium, fontSize: 32, lineHeight: 40, fontWeight: '500' },
});

Spacings.loadSpacings({
  page: 20,
  card: 12,
  gridGutter: 16,
  button: 10,
});

ThemeManager.setComponentTheme('Card', (props: any, _context: any) => {
  const { quickAction, overviewItem, stock, range, news } = props;
  const cardScheme = {
    enableShadow: true,
    padding: 16,
    style: {
      backgroundColor: Colors.portfolioItemBg,
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,
      elevation: 5,
      marginVertical: 16,
    },
  };
  if (quickAction) {
    cardScheme.style = {
      width: 140,
      height: 120,
      backgroundColor: Colors.portfolioItemBg,
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,
      elevation: 5,
      marginVertical: 16,
    };
  }
  if (overviewItem) {
    cardScheme.style = {
      borderRadius: 8,
      height: 72,
      shadowColor: 'rgba(12,91,156,0.4)',
      shadowOffset: {
        width: 0,
        height: 1,
      },
      shadowOpacity: 0.36,
      shadowRadius: 4.68,
      elevation: 11,
      backgroundColor: Colors.portfolioCardBg,
    };
  }
  if (stock) {
    cardScheme.padding = 0;
    cardScheme.style = {
      backgroundColor: Colors.portfolioItemBg,
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,
      elevation: 5,
    };
  }
  if (range) {
    cardScheme.padding = 10;
    cardScheme.style = {
      backgroundColor: Colors.portfolioItemBg,
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,
      elevation: 5,
    };
  }
  if (news) {
    cardScheme.padding = 0;
    cardScheme.style = {
      borderRadius: 12,
      elevation: 5,
    };
  }

  return cardScheme;
});

ThemeManager.setComponentTheme('Image', (props: any, _context: any) => {
  const { garantiLogo } = props;
  const logoScheme = {
    enableShadow: false,
    style: {},
  };
  if (garantiLogo) {
    logoScheme.style = {
      width: 16,
      height: 16,
      borderRadius: 3,
      marginRight: 10,
      // marginVertical: 16,
    };
  }
  return logoScheme;
});

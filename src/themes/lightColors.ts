export default {
  // ==========LIGHT THEME COLORS==========
  // Background = Bg
  // Gradient = Gr
  // ..Dark = Dark Theme
  // ..80Dark = %80 Opacity
  // Herhangi bir sayi yoksa %100 Opacity

  // Main Colors

  action1Light: '#2DCCCD', // Action
  action1_50Light: 'rgba(45, 204, 205, 0.5)', // Action 50%
  action2Light: '#02A5A5', // Action 2
  action3Light: '#0688B8', // Action 3
  action4_5Light: 'rgba(45, 204, 205, 0.05)', // Action 4
  labelGrStartLight: '#5BBEFF', // Label Gradient Start
  labelGrEndLight: '#5AC4C4', // Label Gradient End
  garantiMainLight: '#15181F', // Garanti Main

  // Activity Colors

  greenStatus1Light: '#019F21', // Green Status 1
  greenStatus2Light: '#45E46B', // Green Status 2
  greenButtonLight: '#40B457', // Green Button
  greenStatus1_20Light: 'rgba(1, 159, 33, 0.2)', // Green Status 1 20%
  pressedGreenButton40Light: 'rgba(1, 159, 33, 0.4)', // Pressed Green Button 40% opacity
  disabledGreenButton5Light: 'rgba(1, 159, 33, 0.05)', // Disabled Green Button 5% opacity
  redStatusLight: '#EF2E2E', // Red Status 1
  redButtonLight: '#ED5F4D', // Red Button
  redStatus_20Light: 'rgba(239, 46, 46, 0.2)', // Red Status 1 20% opacity
  pressedRedButton40Light: 'rgba(239, 46, 46, 0.4)', // Pressed Red Button 40% opacity
  disabledRedButton5Light: 'rgba(239, 46, 46, 0.05)', // Disabled Red Button 5% opacity
  orangeStatusLight: '#F7893B', // Orange Status
  yellowStatusLight: '#F8CD51', // Yellow Status
  greyStatusLight: '#BDBDBD', // Grey Status
  darkBlue40Light: 'rgba(38, 77, 135, 0.4)', // Dark Blue
  darkBlue30Light: 'rgba(38, 77, 135, 0.3)', // Dark Blue 30% opacity
  darkBlue15Light: 'rgba(38, 77, 135, 0.15)', // Dark Blue 15% opacity
  disabledStroke5Light: 'rgba(7, 33, 70, 0.05)', // Disabled Stroke 5% opacity

  // Background Colors

  primaryBgLight: '#F2F7F2', // Primary Background
  secondaryBgStartLight: '#004481', // Secondary Background
  secondaryBgEndLight: '#1464A5', // Secondary Background
  selectedBlockStartLight: '#0F5AA0', // Selected Block
  selectedBlockEndLight: '#3978C1', // Selected Block
  clickableBlockBgStartLight: '#EBF7F9', // Clickable Block Background
  clickableBlockBgEndLight: '#F7FAFC', // Clickable Block Background
  clickableCardBgLight: '#F4FBFF', // Clickable Card Background
  menuBg10Light: 'rgba(230, 242, 248, 0.1)', // Menu Background

  // Text Colors

  primaryTextLight: '#072146', // Primary Text
  secondaryText64Light: 'rgba(7, 33, 70, 0.64)', // Secondary Text

  // White Colors
  whiteLight: '#FFFFFF',
  white64Light: 'rgba(255, 255, 255, 0.64)',
  // Other Colors

  softBlueVioletGrStartLight: '#0D76D3', // Soft Blue Violet Gradient Start
  softBlueVioletGrEndLight: '#A282FF', // Soft Blue Violet Gradient End
  blueVioletGrStartLight: '#0A6DC6', // Blue Violet Gradient Start
  blueVioletGrEndLight: '#8263D9', // Blue Violet Gradient End
  blueVioletGrStart70Light: 'rgba(10, 109, 198, 0.7)', // Blue Violet Gradient Start 70%
  blueVioletGrEnd70Light: 'rgba(130, 99, 217, 0.7)', // Blue Violet Gradient End 70%
  blueVioletGrStart50Light: 'rgba(10, 109, 198, 0.5)', // Blue Violet Gradient Start 50%
  blueVioletGrEnd50Light: 'rgba(130, 99, 217, 0.5)', // Blue Violet Gradient End 50%
  inputStroke10Light: 'rgba(7, 33, 70, 0.1)', // Input Stroke
  pressedIconButtonLight: '#D3EFF2', // Pressed Icon Button
  pasificBlueLight: '#2A9BCC', // Pacific Blue
  seaBlueLight: '#016A97', // Sea Blue

  // Top 5 Buyers Portfolio Profit

  celadonLight: '#AFEFBD', // Celadon
  middleGreenLight: '#41A25C', // Middle Green
  darkSpringGreenLight: '#126637', // Dark Spring Green
  lightSeaGreenLight: '#26B99F', // Light Sea Green
  darkSeaBlueLight: '#017D85', // Dark Sea Blue
  jungleMistLight: '#B4D2DE', // Jungle Mist

  // Top 5 Sellers

  blondLight: '#F8E99F', // Blond
  mustardLight: '#FDCE55', // Mustard
  deepSaffronLight: '#FF9F2E', // Deep Saffron
  metallicOrangeLight: '#D86008', // Metallic Orange
  brownLight: '#A43D03', // Brown
  foggyGreyLight: '#D8C7C3', // Foggy Grey

  // Portfolio Loss

  blushLight: '#FC9A8D', // Blush
  lavaLight: '#CF210A', // Lava
  firebrickLight: '#940101', // Firebrick
  orangeLight: '#EF790C', // Orange
  // brownLight: '#A43D03', // Brown
  // foggyGreyLight: '#D8C7C3', // Foggy Grey   -> top 5 sellers tanimlandi

  // Portfolio Account Composition Custodian Analysis

  skyBlueLight: '#95E3FC', // Sky Blue
  jeansBlueLight: '#4CABFF', // Jeans Blue
  sapphireBlueLight: '#3160D8', // Sapphire Blue
  crocusLight: '#A27CF2', // Crocus
  blueberryLight: '#7027B9', // Blueberry
  mauveLight: '#D1B9EA', // Mauve

  // Foreign Currency  Information

  eucalyptusLight: '#47D8A4', // Eucalyptus
  greeenGrStart20Light: 'rgba(71, 216, 164, 0.2)', // Green Gradient Start 20%
  greenGrEndLight: 'rgba(71, 216, 164, 0)', // Green Gradient End
  lavenderLight: '#9747FF', // Lavender
  violetGrStart20Light: 'rgba(151, 71, 255, 0.2)', // Violet Gradient Start 20%
  violetGrEndLight: 'rgba(162, 124, 242, 0)', // Violet Gradient End
  pictonBlueLight: '#43C0EC', // Picton Blue
  blueGrStart20Light: 'rgba(76, 201, 255, 0.2)', // Blue Gradient Start 20%
  blueGrEndLight: 'rgba(76, 201, 255, 0)', // Blue Gradient End

  // Symbol Details

  // action2Light: '#F8CD51', // Action 2
  persianGreenGrStart30Light: 'rgba(45, 204, 205, 0.3)', // Persian Green
  persianGreenGrEndLight: 'rgba(45, 204, 205, 0)', // Persian Green

  // Income Statement

  // skyBlueLight: '#95E3FC', // Sky Blue -> daha onceden tanimlandi
  // seaBlueLight: '#016A97', // Sea Blue -> daha onceden tanimlandi
  // jungleMisLight: '#B4D2DE', // Jungle Mist  -> daha onceden tanimlandi

  // Balance Sheet

  // celadoLight: '#AFEFBD', // Celadon  -> daha onceden tanimlandi
  // lightSeaGreenLight: '#26B99F', // Light Sea Green -> daha onceden tanimlandi
  // crocusLight: '#A27CF2', // Crocus   -> daha onceden tanimlandi

  // Cash Flow

  // mauveLight: '#D1B9EA', // Mauve -> daha onceden tanimlandi          -> daha onceden tanimlandi
  // crocusLight: '#A27CF2', // Crocus -> daha onceden tanimlandi -> daha onceden tanimlandi
  // blueberryLight: '#7027B9', // Blueberry -> daha onceden tanimlandi -> daha onceden tanimlandi

  // Financial Evaluation

  // mustardLight: '#FDCE55', // Mustard -> daha onceden tanimlandi         -> daha onceden tanimlandi
  // deepSaffronLight: '#FF9F2E', // Deep Saffron -> daha onceden tanimlandi-> daha onceden tanimlandi
  // middleGreenLight: '#41A25C', // Middle Green -> daha onceden tanimlandi-> daha onceden tanimlandi

  // Multiples Ratios

  greenOrangeGrStartLight: 'rgba(64, 180, 87, 1)', // Green Orange Gradient Start
  greenOrangeGrMidLight: 'rgba(214, 187, 50, 1)', // Green Orange Gradient End
  greenOrangeGrEndLight: 'rgba(246, 70, 70, 1)', // Green Orange Gradient End

  orangeGreenGrStartLight: 'rgba(246, 70, 70, 1)', // Orange Green Gradient Start
  orangeGreenGrMidLight: 'rgba(214, 187, 50, 1)', // Orange Green Gradient Mid
  orangeGreenGrEndLight: 'rgba(64, 180, 87, 1)', // Orange Green Gradient End

  // Dev Colors
  warn: '#FF963C',

  //modal içeriklerinden

  mainAction10: 'rgba(2, 165, 165, 0.1)',
  //(15.36deg, #004481 21.55%, #1464A5 78.45%)
  modalGradientBlueLight22: 'rgba(0, 68, 129, 22)',
  modalGradinetBlueLight78: 'rgba(20, 100, 165, 78)',

  modalGradientBlueDark: '#040B2F',
  action50Light:"#fff",
};

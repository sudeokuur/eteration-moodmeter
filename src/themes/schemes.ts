import { DarkTheme, DefaultTheme } from '@react-navigation/native';
import { Colors } from 'react-native-ui-lib';

// WLcardBgLight:"#F2F7FA",
// WLloseArrowLight:"#EF2E2E",
// WLupArrowLight:"#019F21",

const lightThemeColors = {
  warn: Colors.warn,
  background: Colors.primaryBgLight,
  secondaryBgStart: Colors.secondaryBgStartLight, // Light tema ust mavi ekran
  secondaryBgEnd: Colors.secondaryBgEndLight, // Light tema ust mavi ekran
  primaryText: Colors.primaryTextLight,
  secondaryText: Colors.secondaryText64Light,
  secondaryTextBlueBg: Colors.white64Light,
  white: Colors.whiteLight,
  white64: Colors.secondaryText64Dark, // Light tema mavi ekranda total equity gri alanlar icin tanimlandi.
  // LIGHT
  action: Colors.action1Light,
  notificationCardStart: Colors.clickableBlockBgStartLight,
  notificationCardEnd: Colors.clickableBlockBgStartLight,
  portfolioItemBg: Colors.white, // overview ekranindaki hisse listesi bg
  greenPrice: Colors.greenStatus1Light, // hisse listesindeki fiyatlarin rengi
  redPrice: Colors.redStatusLight, // hisse listesindeki fiyatlarin rengi
  portfolioCardBg: Colors.white, // portfolio ekranindaki tiklanabilir kartlarin background
  

  greenButton: Colors.greenButtonLight,
  redButton: Colors.redButtonLight,
  alertButton: Colors.blue50Dark,
  // icon Bg
  infoIconBg: Colors.secondaryText64Light,
  crossIconBg: Colors.white,
  crossIconColor: Colors.secondaryText64Light,
  rightArrowColor: Colors.secondaryText64Light,

  // Button
  $btnPrimary: '#02A5A5',
  $btnPrimaryDisabled: 'rgba(7, 33, 70, 0.1)',
  $btnPrimaryText: '#FFFFFF',
  $btnPrimaryPressedBackground: 'rgba(45, 204, 205, 0.15)',
  $btnPrimaryPressedText: '#2DCCCD',
  // Button Outline
  $btnOutlineText: '#02A5A5',
  $btnOutlinePressedBackground: 'rgba(45, 204, 205, 0.15)',
  $btnOutlineDisabledText: 'rgba(38, 77, 135, 0.4)',
  // Button Outline Secondary
  $btnOutlineSecondary: '#2DCCCD',
  $btnOutlineSecondaryIcon: '#02A5A5',
  $btnOutlineSecondaryPressedIcon: Colors.actionDark,
  $btnOutlineSecondaryDisabledIcon: Colors.darkBlue40Dark,
  // Button Success
  $btnSuccess: '#40B457',
  $btnSuccessPressed: Colors.middleGreenLight,
  $btnSuccessDisabled: 'rgba(1, 159, 33, 0.2)',
  $btnSuccessLabelColor: Colors.white,
  $btnSuccessDescriptionColor: Colors.white,
  // Button Danger
  $btnDanger: '#ED5F4D',
  $btnDangerPressed: '#EF2E2E',
  $btnDangerDisabled: 'rgba(239, 46, 46, 0.2)',

  $btnDSDisabledLabelColor: Colors.white,
  // Button Secondary
  $btnSecondary: 'rgba(2, 165, 165, 0.1)',
  $btnSecondaryPressed: 'rgba(45, 204, 205, 0.15)',
  $btnSecondaryText: '#072146',
  $btnSecondaryPressedText: '#02A5A5',
  $btnSecondaryDisabled: 'rgba(7, 33, 70, 0.1)',
  $btnSecondaryDisabledText: 'rgba(255, 255, 255, 0.64)',
  $btnSecondaryIcon: '#02A5A5',

  // TextField
  $inputBorderColor: Colors.darkBlue30Light,
  $inputBorderColorError: Colors.redStatusLight,
  $inputLabelColor: Colors.secondaryText64Light,
  $inputPlaceHolderColor: Colors.secondaryText64Light,
  $inputTextColor: Colors.primaryTextLight,
  $inputCounterColor: Colors.secondaryText64Light,
  $inputValidationColor: Colors.redStatusLight,
  $inputFocusedBorderColor: Colors.darkBlue40Light,

  $inputPlusMinusIconColor: Colors.action2Light,
  $inputIncreaseBackgroundColor: 'rgba(2, 165, 165, 0.1)',

  $inputCurrencyColor: Colors.secondaryText64Light,
  watchListQuickItemBackground: Colors.white,

  // Checkbox

  $checkboxBg: '#02A5A5',
  $checkboxIconColor: '#FFFFFF',
  $checkboxSecondaryIconColor: 'rgba(7, 33, 70, 0.64)',
  $checkboxBorderColor: 'rgba(38, 77, 135, 0.3)',
  $checkboxContainerBg: 'rgba(2, 165, 165, 0.1)',
  $checkboxContainerBgDisabled: 'rgba(7, 33, 70, 0.05)',
  $checkboxPrimaryTextActive: Colors.primaryTextLight,
  $checkboxSecondaryTextActive: Colors.secondaryText64Light,
  $checkboxPrimaryTextInActive: Colors.secondaryText64Light,
  $checkboxSecondaryTextInActive: Colors.secondaryText64Light,
  $checkboxThirdText: 'rgba(38, 77, 135, 0.3)',

  //modal
  modalBgBlue22: Colors.modalGradientBlueLight22,
  modalBgBlue78: Colors.modalGradientBlueLight78,

  //watclist
  filterItemBg:Colors.filterItemBgLight,
  action50:Colors.action50Light,
  searchBorderWidth:Colors.inputStroke10Light
};

///////////////////////////////////////////////

/////////////    DARK  DARK DARK DARK DARK DARK/////

////////////////////////////////////////////////

const darkThemeColors = {
  warn: Colors.warn,
  background: Colors.primaryBgDark,
  secondaryBgStart: Colors.primaryBgDark,
  secondaryBgEnd: Colors.primaryBgDark,
  primaryText: Colors.primaryTextDark,
  secondaryText: Colors.secondaryText64Dark,
  secondaryTextBlueBg: Colors.secondaryText64Dark,
  white: Colors.whiteDark,
  white64: Colors.secondaryText64Dark,

  // DARK
  action: Colors.actionDark,
  notificationCardStart: Colors.clickableBlockBgStartDark,
  notificationCardEnd: Colors.clickableBlockBgStartDark,
  portfolioItemBg: Colors.blockBg8Dark, // overview ekranindaki hisse listesi bg
  greenPrice: Colors.greenStatusDark,
  redPrice: Colors.redStatusDark,
  // portfolio ekranindaki grafik background

  portfolioCardBg: Colors.blockBg8Dark, // portfolio ekranindaki tiklanabilir kartlarin background

  greenButton: Colors.greenButtonDark,
  redButton: Colors.redButtonDark,
  alertButton: Colors.blue50Dark,

  //icon Bg

  infoIconBg: Colors.secondaryText64Dark,
  crossIconBg: Colors.white20Dark,
  crossIconColor: Colors.secondaryText64Dark,
  rightArrowColor: Colors.secondaryText64Dark,

  // Button
  $btnPrimary: '#2DCCCD',
  $btnPrimaryDisabled: 'rgba(255, 255, 255, 0.2)',
  $btnPrimaryText: '#040B2F',
  $btnPrimaryPressedBackground: 'rgba(45, 204, 205, 0.2)',
  $btnPrimaryPressedText: '#2DCCCD',
  // Button Outline
  $btnOutlineText: '#FFFFFF',
  $btnOutlinePressedBackground: 'rgba(45, 204, 205, 0.1)',
  $btnOutlineDisabledText: Colors.white30Dark,
  // Button Outline Secondary
  $btnOutlineSecondary: Colors.white30Dark,
  $btnOutlineSecondaryIcon: 'rgba(255, 255, 255, 0.64)',
  $btnOutlineSecondaryPressedIcon: Colors.white,
  $btnOutlineSecondaryDisabledIcon: Colors.white30Dark,
  // Button Success
  $btnSuccess: '#2D8742',
  $btnSuccessPressed: Colors.darkSpringGreenDark,
  $btnSuccessDisabled: 'rgba(18, 102, 55, 0.5)',
  $btnSuccessLabelColor: Colors.white,
  $btnSuccessDescriptionColor: Colors.secondaryText64Dark,
  // Button Danger
  $btnDanger: '#982E20',
  $btnDangerPressed: '#572222',
  $btnDangerDisabled: 'rgba(87, 34, 34, 0.5)',

  $btnDSDisabledLabelColor: Colors.white30Dark,
  // Button Secondary
  $btnSecondary: 'rgba(255, 255, 255, 0.08)',
  $btnSecondaryPressed: 'rgba(45, 204, 205, 0.2)',
  $btnSecondaryText: Colors.white,
  $btnSecondaryPressedText: '#2DCCCD',
  $btnSecondaryDisabled: 'rgba(255, 255, 255, 0.08)',
  $btnSecondaryDisabledText: Colors.white30Dark,
  $btnSecondaryIcon: '#2DCCCD',

  // TextFields
  $inputBorderColor: Colors.white30Dark,
  $inputBorderColorError: Colors.redStatusDark,
  $inputLabelColor: Colors.secondaryText64Dark,
  $inputPlaceHolderColor: Colors.white30dark,
  $inputTextColor: Colors.white,
  $inputCounterColor: Colors.secondaryText64Dark,
  $inputValidationColor: Colors.redStatusDark,
  $inputFocusedBorderColor: Colors.secondaryText64Dark,

  $inputPlusMinusIconColor: Colors.actionDark,
  $inputIncreaseBackgroundColor: 'rgba(255, 255, 255, 0.08)',

  $inputCurrencyColor: Colors.secondaryText64Dark,

  watchListQuickItemBackground: Colors.blockBg8Dark,

  // Checkbox colors
  $checkboxBg: 'rgba(45, 204, 205, 0.5)',
  $checkboxIconColor: Colors.white,
  $checkboxSecondaryIconColor: Colors.secondaryText64Dark,
  $checkboxBorderColor: 'rgba(38, 77, 135, 0.3)',
  $checkboxContainerBg: 'rgba(45, 204, 205, 0.1)',
  $checkboxContainerBgDisabled: 'rgba(255, 255, 255, 0.04)',
  $checkboxPrimaryTextActive: Colors.white,
  $checkboxSecondaryTextActive: Colors.secondaryText64Dark,
  $checkboxPrimaryTextInActive: Colors.secondaryText64Dark,
  $checkboxSecondaryTextInActive: Colors.secondaryText64Dark,
  $checkboxThirdText: 'rgba(38, 77, 135, 0.3)',

  //modal

  modalBgBlue22: Colors.modalGradientBlueDark,
  modalBgBlue78: Colors.modalGradientBlueDark,


  //watclist
  filterItemBg:Colors.tabBgGrStart8Dark,
  action50:Colors.action50Dark,
  searchBorderWidth:Colors.white20Dark
};

const lightTheme = {
  // ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    ...lightThemeColors,
  },
};

const darkTheme = {
  // ...DarkTheme,
  colors: {
    ...DarkTheme.colors,
    ...darkThemeColors,
  },
};

export { lightTheme, darkTheme };

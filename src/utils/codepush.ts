import CodePush from 'react-native-code-push';

export const codePushStatusDidChange = (dropdownAlert: any) => (syncStatus: any) => {
  switch (syncStatus) {
    case CodePush.SyncStatus.CHECKING_FOR_UPDATE:
      console.log('CHECKING_FOR_UPDATE');
      break;
    case CodePush.SyncStatus.DOWNLOADING_PACKAGE:
      dropdownAlert.current?.alertWithType('success', '', 'Uygulama güncellemesi indiriliyor');
      break;
    case CodePush.SyncStatus.AWAITING_USER_ACTION:
      console.log('AWAITING_USER_ACTION');
      break;
    case CodePush.SyncStatus.INSTALLING_UPDATE:
      dropdownAlert.current?.alertWithType('success', '', 'Uygulama güncellemesi yükleniyor');
      break;
    case CodePush.SyncStatus.UP_TO_DATE:
      console.log('UP_TO_DATE');
      break;
    case CodePush.SyncStatus.UPDATE_IGNORED:
      console.log('UPDATE_IGNORED');
      break;
    case CodePush.SyncStatus.UPDATE_INSTALLED:
      dropdownAlert.current?.alertWithType(
        'success',
        '',
        'Uygulama güncellemesi başarı ile yüklendi',
      );
      break;
    case CodePush.SyncStatus.UNKNOWN_ERROR:
      dropdownAlert.current?.alertWithType('error', '', 'Uygulama güncellemesi başarısız oldu');
      break;
  }
};

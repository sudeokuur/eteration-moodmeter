import analytics from '@react-native-firebase/analytics';
import crashlytics from '@react-native-firebase/crashlytics';
import messaging from '@react-native-firebase/messaging';

export function pageVisitAnalytics(routeNameRef: any, navigationRef: any) {
  return async () => {
    const previousRouteName = routeNameRef.current;
    const nav: any = navigationRef.current;
    const currentRouteName = nav?.getCurrentRoute().name;

    if (previousRouteName !== currentRouteName) {
      await analytics().logScreenView({
        screen_name: currentRouteName,
        screen_class: currentRouteName,
      });
    }
    routeNameRef.current = currentRouteName;
  };
}

export async function onSignIn(user: any) {
  crashlytics().log('User signed in.');
  await Promise.all([
    crashlytics().setUserId(user.portiaId),
    crashlytics().setAttributes({
      mobile: user.mobile,
      username: user.preferred_username,
    }),
  ]);
}

export async function getDeviceToken() {
  if (!messaging().isDeviceRegisteredForRemoteMessages) {
    messaging()
      .registerDeviceForRemoteMessages()
      .then(() => {
        messaging()
          .getToken()
          .then((token) => {
            console.log('FCM Token: ', token);
          })
          .catch((e) => {
            console.log(e);
          });
      });
  } else {
    messaging()
      .getToken()
      .then((token) => {
        console.log('FCM Token: ', token);
      })
      .catch((e) => {
        console.log(e);
      });
  }
}

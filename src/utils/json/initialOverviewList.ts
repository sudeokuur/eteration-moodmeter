export default [
  {
    key: 'portfolioSummary',
    label: 'Portfolio performance summary',
    changeable: false,
    selected: true,
  },
  {
    key: 'notificationCenter',
    label: 'Notification center',
    changeable: false,
    selected: true,
  },
  {
    key: 'watchlist',
    label: 'Watchlist',
    changeable: true,
    selected: true,
  },
  {
    key: 'marketGraphs',
    label: 'Market graphs',
    changeable: true,
    selected: false,
  },
  {
    key: 'news',
    label: 'News',
    changeable: true,
    selected: false,
  },
  {
    key: 'researchReports',
    label: 'Research Reports',
    changeable: true,
    selected: false,
  },
];

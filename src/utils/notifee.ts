import notifee, { EventType } from '@notifee/react-native';
import { trim } from 'lodash';

export async function onDisplayNotification({ t, navigation, payload }: any) {
  // Request permissions (required for iOS)
  await notifee.requestPermission();

  if (!payload) {
    return;
  }

  // Create a channel (required for Android)
  const channelId = await notifee.createChannel({
    id: 'default',
    name: 'Default Channel',
  });

  let bodyMessage = '';

  if (payload.title === 'Order Process Message' || payload.title === 'Process Message') {
    var data = payload?.body.split(':');
    bodyMessage = `${t(data[0])}: ${t(trim(data[1]))}`;
  } else if (payload?.body.indexOf('from') !== -1) {
    var data = payload?.body.split(' ');
    bodyMessage = `${t('oldStatu')}:${t(data[2])}-${t('newStatu')}:${t(data[0])}`;
  } else {
    bodyMessage = t(payload?.body);
  }
  notifee.onForegroundEvent(({ type }) => {
    switch (type) {
      case EventType.PRESS:
        navigation?.navigate('OrderListScreen' as never, {} as never);
        break;
    }
  });

  // Display a notification
  await notifee.displayNotification({
    title: t(payload?.title) || '...',
    body: bodyMessage || '...',
    android: {
      channelId,
      // smallIcon: 'name-of-a-small-icon', // optional, defaults to 'ic_launcher'.
      // pressAction is needed if you want the notification to open the app when pressed
      pressAction: {
        id: 'default',
      },
    },
  });
}

import AsyncStorage from '@react-native-async-storage/async-storage';
import _ from 'lodash';

const setStorage = async (key, value) => {
  AsyncStorage.setItem(key, JSON.stringify(value));
};

const getStorage = async key => {
  const response = await AsyncStorage.getItem(key);
  if (response !== null) {
    return JSON.parse(response);
  }
};

const getAllStorage = async keys => {
  const storage = [];
  const response = await AsyncStorage.multiGet(keys);
  if (response !== null) {
    _.each(response, item => {
      storage.push({ key: item[0], value: JSON.parse(item[1]) });
    });
    return storage;
  }
};

export { getStorage, setStorage, getAllStorage };

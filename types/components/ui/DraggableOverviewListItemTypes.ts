export type DraggableOverviewListItemTypes = {
  item: {
    key: string;
    label: string;
    changeable: boolean;
    selected: boolean;
  };
  drag: Function;
  isActive: boolean;
  type: string;
  onItemAddDeselectedList?: Function;
  onItemAddSelectedList?: Function;
};

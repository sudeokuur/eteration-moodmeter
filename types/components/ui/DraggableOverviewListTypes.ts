import { DraggableOverviewListItemTypes } from './DraggableOverviewListItemTypes';

export type DraggableOverviewListTypes = {
  list: DraggableOverviewListItemTypes[];
  type: string;
  onChangeList?: Function;
  onItemAddSelectedList?: Function;
  onItemAddDeselectedList?: Function;
};
